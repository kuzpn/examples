unit f_Log;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfLog = class(TForm)
    memo: TMemo;
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure WriteLog(str: string);
  end;

  procedure Log(str: string);

var
  fLog: TfLog;

implementation

{$R *.dfm}

class procedure TfLog.WriteLog(str: string);
begin
  if fLog = nil then
    fLog:= TfLog.Create(Application.MainForm);

  if not fLog.Visible then
    fLog.Show;
    
  fLog.memo.Lines.Add(str);
end;

procedure Log(str: string);
begin
{$IFDEF DEBUG}
  TfLog.WriteLog(str);
{$ENDIF}
end;

end.
