object fMain: TfMain
  Left = 285
  Top = 113
  Width = 665
  Height = 481
  Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1087#1086' '#1074#1072#1075#1086#1085#1072#1084
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 300
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  DesignSize = (
    657
    447)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 29
    Height = 13
    Caption = #1060#1072#1081#1083
  end
  object edFile: TEdit
    Left = 64
    Top = 16
    Width = 489
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    ReadOnly = True
    TabOrder = 0
  end
  object btSel: TButton
    Left = 568
    Top = 16
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = '&'#1042#1099#1073#1088#1072#1090#1100'...'
    TabOrder = 1
    OnClick = btSelClick
  end
  object btExit: TButton
    Left = 552
    Top = 408
    Width = 89
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1042#1099#1081#1090#1080
    TabOrder = 2
    OnClick = btExitClick
  end
  object btSave: TButton
    Left = 432
    Top = 408
    Width = 107
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082'...'
    Enabled = False
    TabOrder = 3
    OnClick = btSaveClick
  end
  object grid: TDrawGrid
    Left = 16
    Top = 56
    Width = 625
    Height = 337
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 22
    DefaultRowHeight = 26
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing]
    TabOrder = 4
    OnClick = gridClick
    OnDrawCell = gridDrawCell
    OnGetEditMask = gridGetEditMask
    OnGetEditText = gridGetEditText
    OnSelectCell = gridSelectCell
    OnSetEditText = gridSetEditText
  end
  object XPManifest1: TXPManifest
    Left = 16
    Top = 408
  end
  object dlgOpen: TOpenDialog
    DefaultExt = '*.txt'
    Filter = #1058#1077#1082#1089#1090#1086#1074#1099#1077' '#1092#1072#1081#1083#1099' (*.txt)|*.txt|'#1042#1089#1077' '#1092#1072#1081#1083#1099' (*.*)|*.*'
    Options = [ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 56
    Top = 408
  end
  object dlgSave: TSaveDialog
    DefaultExt = '*.csv'
    Filter = 
      #1058#1077#1082#1089#1090#1086#1074#1099#1077' '#1092#1072#1081#1083#1099' '#1089' '#1088#1072#1079#1076#1077#1083#1080#1090#1077#1083#1103#1084#1080' (*.csv)|*.csv|'#1058#1077#1082#1089#1090#1086#1074#1099#1077' '#1092#1072#1081#1083#1099' (*' +
      '.txt)|*.txt|'#1042#1089#1077' '#1092#1072#1081#1083#1099' (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 96
    Top = 408
  end
end
