unit u_Parser;

interface
  uses IniFiles, SysUtils, StrUtils, f_Log;

  const
    cListTankNums = '�����(�)';
    cSecTechPasspBeg = '***************************************************************';
    cSecSeparator = '---------------------------------------------------------------';
    cSecTechPasspTitle = 'TEXH��ECK��  � A C � O P T  BA�OHA N';
    cSecTechPasspOwner = 'CO�CTBEHH�K';
    cSecTechPasspCompany = '�PE��P��T�E';
    cSecTechPasspManufacturer = '���OTOB�TE��';
    cSecTechPasspRepairs = '� O C � E � H � E   P E M O H T �';
    cSecTechPasspHeavy = 'KA��TA��H��';
    cSecTechPasspRailroad = '�E�OBCKO�';
    cSecTechPasspEnd = '===============================================================';    
    cSecExtSep = 'BA�OH';
    cSecExtNextRepair = 'C�E�.PEMOHT:';
    cSecExtFabricate = '�OCTPOEH';
    cSecExtHeavyInfo = 'KA��TA��H�� -';
    cSecExtRailroadInfo = '�E�OBCKO�   -';

type
  SectionType = (secGeneral, secTechPassp, secTechPasspRepairs,
                  secTechPasspHeavy, secTechPasspRailroad, secTechPasspExt);

  TTank = class(TObject)
  public
    num: string;                    // ����� ������
    owner: string;                  // ��������. ������������
    owner_code: string;             // ��������. ���
    fabrication_date: string;       // ���� ���������
    manufacturer: string;           // ������������ (����� ���������)
    rent_date: string;              // ���� ��������� ������

    // ��
    heavy_repair_date: string;      // ���� ��
    heavy_repair_place: string;     // ����� ��
    heavy_repair_code: string;      // ��� ����������� ������������ ������
    check_heavy_date: string;       // ���� �������������������
    check_heavy_place: string;      // ����� �������������������
    next_heavy_date: string;        // ���� ���������� ��

    // ��
    railroad_repair_date: string;   // ���� ��
    railroad_repair_place: string;  // ����� ��
    railroad_repair_code: string;   // ��� ����������� ������������ ������
    check_railroad_date: string;    // ���� �������������������
    check_railroad_place: string;   // ����� �������������������
    next_railroad_date: string;     // ���� ���������� ��

    tech_obslug_date: string;       // ���� ���������������
    tech_obslug_place: string;      // ����� ���������������

    constructor Create(num: string);
  private
  end;

  TFileParser = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;

    procedure ClearData;
    function StartParse(fileName: string): integer;
    procedure ParseLine(str: string);
    procedure ParseListTankNums(str: string);
    procedure ParseTechPasspTitle(str: string);
    function ParseDate(str: string; var date: TDateTime): boolean;
    procedure ParseTechPasspOwner(str: string);
    procedure ParseTechPasspCompany(str: string);    
    procedure ParseTechPasspManufacturer(str: string);
    procedure ParseHeavyDate(str: string);
    procedure ParseHeavyPlace(str: string);
    procedure ParseRailroadDate(str: string);
    procedure ParseRailroadPlace(str: string);
    procedure ParseExtTankNum(str: string);
    procedure ParseExtNextRepair(str: string);
    procedure ParseExtFabricate(str: string);
    procedure ParseExtRepairInfo(str: string; rep_type: integer);        

  private
    fileName: string;
    tanks: THashedStringList;
    curSec: SectionType;
    curTank: TTank;

  public
    property TankList: THashedStringList read tanks;

  end;

  function StrBeginOn(full_str: string; left_str: string): boolean;
  function RemoveLeftStr(full_str: string; left_str: string): string;
  function RemoveToStr(full_str: string; to_str: string): string;
  function GetFirstWord(full_str: string; delimiter: string): string;
  function IsNumber(ch: char): boolean;
  function IsLetter(ch: char): boolean;
  function ConvertEngToRus(str: string): string;  
  function StrToNumber(str: string): integer;
  function StrToMonth(str: string): integer;

implementation

uses Classes;

function StrBeginOn(full_str: string; left_str: string): boolean;
begin
  Result:= LeftStr(full_str, Length(left_str)) = left_str;
end;

function RemoveLeftStr(full_str: string; left_str: string): string;
  var left_str_len: integer;
begin
  left_str_len:= Length(left_str);
  Result:= TrimLeft(
      MidStr( full_str, left_str_len+1,
          Length(full_str) - left_str_len )
    );
end;

function RemoveToStr(full_str: string; to_str: string): string;
  var to_str_len: integer;
      cpos: integer;
      res: string;
begin
  to_str_len:= Length(to_str);
  cpos:= Pos(to_str, full_str);
  if cpos > 0 then
    begin
      cpos:= cpos + to_str_len;
      res:= TrimLeft(
          MidStr( full_str, cpos,
              Length(full_str) - cpos + 1 )
        );
    end
  else
    res:= full_str;

  Result:= res;
end;

function GetFirstWord(full_str: string; delimiter: string): string;
  var cpos: integer;
      res: string; 
begin
  cpos:= Pos(delimiter, full_str);
  if cpos > 0 then
    begin
      res:= LeftStr(full_str, cpos-1);
    end
  else
    res:= full_str;

  Result:= res;
end;

function IsNumber(ch: char): boolean;
begin
  Result:= ch in ['0'..'9'];
end;

function IsLetter(ch: char): boolean;
begin
  Result:= (ch in ['�'..'�']) or (ch in ['�'..'�']);
end;

// �������� ����� ����.�������� �� �������.�������
function ConvertEngToRus(str: string): string;
  type ArrLettersItem = array [1..2] of char;
  const letters: array [1..10] of ArrLettersItem = (
      ('A', '�'), // ('EngLetter', 'RusLetter')
      ('B', '�'),
      ('C', '�'),
      ('E', '�'),
      ('H', '�'),
      ('K', '�'),
      ('M', '�'),
      ('O', '�'),
      ('P', '�'),
      ('T', '�')
    );
  var i, l: integer;
begin
  for i:=1 to Length(str) do
    begin
      for l:= 1 to High(letters) do
        if UpperCase(str[i]) = letters[l, 1] then
          str[i]:= letters[l, 2];
    end;
  Result:= str;
end;

function StrToNumber(str: string): integer;
begin
  Result:= StrToInt(str);
end;

function StrToMonth(str: string): integer;
  type ArrMonthItem = array [1..2] of string;
  const months: array [1..12] of ArrMonthItem = (
            ('������',  '������'),
            ('�������', '�������'),
            ('����',    '�����'),
            ('������',  '������'),
            ('���',     '���'),
            ('����',    '����'),
            ('����',    '����'),
            ('������',  '�������'),
            ('��������','��������'),
            ('�������', '�������'),
            ('������',  '������'),
            ('�������', '�������') );
  var i: integer;
      res: integer;
      s: string;
begin
  res:= 0;
  s:= UpperCase(str);
  for i:=1 to 12 do
    begin
      if (s = months[i, 1]) or (s = months[i, 2]) then
        begin
          res:= i;
          break;
        end;
    end;
  if res = 0 then raise Exception.Create('���������� ������������� ����� ''' + str + '');
    
  Result:= res;
end;

//------------------------------------------------------------------------------

constructor TTank.Create(num: string);
begin
  inherited Create;
  Self.num:= num;
end;

constructor TFileParser.Create;
begin
  inherited Create;
  tanks:= THashedStringList.Create;
end;

destructor TFileParser.Destroy;
begin
  ClearData;

  if tanks <> nil then
    tanks.Free;

  inherited Destroy;
end;

function TFileParser.StartParse(fileName: string): integer;
  var str: string;
      hFile: TextFile;
begin
  ClearData;
  if FileExists(fileName) then
    begin
      Self.fileName:= fileName;
      AssignFile(hFile, fileName);
      FileMode:= fmOpenRead;
      Reset(hFile);

      Log('<<< START... >>>');
      while not Eof(hFile) do
        begin
          Readln(hFile, str);
          str:= Trim(str);
          if Length(str) <> 0 then
            ParseLine(str);
        end;
      Log('<<< END. >>>');

      CloseFile(hFile);
    end
  else
    begin
      Self.fileName:= '';
      raise Exception.Create('�� ������ ����: ' + fileName);
    end;

  Result:= tanks.Count;
end;

//------------------------------------------------------------------------------


// ���������� Self.tanks
procedure TFileParser.ParseListTankNums(str: string);
  var s: string;
      tank_num: string;
      cpos: integer;
begin
  s:= RemoveLeftStr(str, cListTankNums);

  Log(cListTankNums);
  repeat
    cpos:= Pos(':', s);
    if cpos > 0 then
      begin
        tank_num:= LeftStr(s, cpos-1);
        s:= TrimLeft(MidStr(s, cpos+1, Length(s)-cpos));
      end
    else
      tank_num:= s;

    if Length(tank_num) > 0 then
      tanks.AddObject(tank_num, TTank.Create(tank_num));

    Log(tank_num);
  until cpos = 0;
end;

// TTank.num
procedure TFileParser.ParseTechPasspTitle(str: string);
  var s: string;
      tank: TTank;
      idx: integer;
begin
  s:= RemoveLeftStr(str, cSecTechPasspTitle);
  Log('������� ������ �' + s);

  idx:= tanks.IndexOf(s);
  if idx < 0 then
    begin
      tank:= TTank.Create(s);
      tanks.AddObject(s, tank);
      Log('�������� � ������ ����� �' + s);
    end
  else
    tank:= TTank(tanks.Objects[idx]);

  Self.curTank:= tank;
end;

function TFileParser.ParseDate(str: string; var date: TDateTime): boolean;
  var res: boolean;
      day: integer;
      month: integer;
      year: integer;
      w: string;
      s: string;
begin
  res:= false;
  s:= ConvertEngToRus(str);
  day:= 0;
  month:= 1;
  year:= 0;
  date:= 0;

  try

    repeat
      w:= GetFirstWord(s, ' ');
      s:= RemoveLeftStr(s, w);

      if IsNumber(w[1]) then
        begin
          if day = 0 then
            day:= StrToNumber(w)
          else
            year:= StrToNumber(w);
        end
      else if w = '--' then
        begin
          if day = 0 then day:= 1;
        end
      else if IsLetter(w[1]) then
        begin
          month:= StrToMonth(w);
          if day = 0 then day:= 1;
        end;
    until (Length(w) = 0) or (year > 0);
    date:= EncodeDate(year, month, day);
    res:= true;

  except
  end;

  Result:= res;
end;

// TTank.fabrication_date
procedure TFileParser.ParseTechPasspOwner(str: string);
  const cL_Fabrication_date = '�OCTPOEH';
  var s: string;
      date: TDateTime;
begin
  s:= RemoveToStr(str, cL_Fabrication_date);
  if (curTank <> nil) and (ParseDate(s, date)) then
      curTank.fabrication_date:= DateToStr(date);
end;

// TTank.owenr_code
// TTank.owner
procedure TFileParser.ParseTechPasspCompany(str: string);
  var s: string;
      w: string;
      cpos: integer;
begin
  s:= RemoveLeftStr(str, cSecTechPasspCompany);
  w:= GetFirstWord(s, ' ');

  if (curTank <> nil) and (Length(w) > 0) then
      curTank.owner_code:= w;

  s:= RemoveLeftStr(s, w);
  cpos:= Pos('(', s);
  if (cpos > 0) then
      s:= Trim( MidStr( s, 1, cpos-1) );
      
  if (curTank <> nil) and (Length(s) > 0) then
    curTank.owner:= s;
end;

// TTank.manufacturer
procedure TFileParser.ParseTechPasspManufacturer(str: string);
  var s: string;
begin
  s:= RemoveLeftStr(str, cSecTechPasspManufacturer);
  if (curTank <> nil) and (Length(s) > 0) then
      curTank.manufacturer:= s;
end;

// TTank.heavy_repair_date
procedure TFileParser.ParseHeavyDate(str: string);
  var s: string;
      date: TDateTime;
begin
  s:= RemoveLeftStr(str, cSecTechPasspHeavy);
  if (curTank <> nil) and (ParseDate(s, date)) then
    begin
      curTank.heavy_repair_date:= DateToStr(date);
      curTank.check_heavy_date:= curTank.heavy_repair_date;
    end;
end;

// TTank.heavy_repair_place
// TTank.heavy_repair_code
procedure TFileParser.ParseHeavyPlace(str: string);
  var s: string;
      w: string;
begin
  w:= GetFirstWord(str, ' ');
  s:= RemoveLeftStr(str, w);
  w:= MidStr(w, 2, Length(w)-2);  // ��� ����������� ������������ ������

  if (curTank <> nil) then
    begin
      curTank.heavy_repair_place:= s;
      curTank.heavy_repair_code:= w;
      curTank.check_heavy_place:= s;
    end;
end;

// TTank.railroad_repair_date
procedure TFileParser.ParseRailroadDate(str: string);
  var s: string;
      date: TDateTime;
begin
  s:= RemoveLeftStr(str, cSecTechPasspHeavy);
  if (curTank <> nil) and (ParseDate(s, date)) then
    begin
      curTank.railroad_repair_date:= DateToStr(date);
      curTank.check_railroad_date:= curTank.railroad_repair_date;
    end;
end;

// TTank.railroad_repair_place
// TTank.railroad_repair_code
procedure TFileParser.ParseRailroadPlace(str: string);
  var s: string;
      w: string;
begin
  w:= GetFirstWord(str, ' ');
  s:= RemoveLeftStr(str, w);
  w:= MidStr(w, 2, Length(w)-2);  // ��� ����������� ������������ ������

  if curTank <> nil then
    begin
      curTank.railroad_repair_place:= s;
      curTank.railroad_repair_code:= w;
      curTank.check_railroad_place:= s;
    end;
end;

// ������������� Self.curTank
procedure TFileParser.ParseExtTankNum(str: string);
  var s: string;
      w: string;
      idx: integer;
      tank: TTank;
begin
  s:= RemoveLeftStr(str, cSecExtSep);
  w:= GetFirstWord(s, ' ');

  if Length(w) > 0 then
    begin
      idx:= tanks.IndexOf(w);
      if idx >= 0 then
        begin
          tank:= TTank(tanks.Objects[idx]);
          if tank <> nil then
            curTank:= tank;
        end
      else
        begin
          tank:= TTank.Create(w);
          tanks.AddObject(w, tank);
          curTank:= tank;
        end;
    end;
end;

// TTank.next_heavy_date
// TTank.next_railroad_date
procedure TFileParser.ParseExtNextRepair(str: string);
  var s: string;
      date: TDateTime;
begin
  s:= RemoveLeftStr(str, cSecExtNextRepair);
  if (curTank <> nil) and (ParseDate(s, date)) then
    begin
      s:= RemoveToStr(s, '�.');
      if s = '�E�OBCKO�' then
        curTank.next_railroad_date:= DateToStr(date)
      else if s = 'KA��TA��H��' then
        curTank.next_heavy_date:= DateToStr(date);
    end;
end;

procedure TFileParser.ParseExtFabricate(str: string);
  var s: string;
      w: string;
      date: TDateTime;
begin
  s:= RemoveLeftStr(str, cSecExtFabricate);
  s:= RemoveLeftStr(s, '--');
  if (curTank <> nil) and (ParseDate(s, date)) then
    curTank.fabrication_date:= DateToStr(date);

  s:= RemoveToStr(s, '�ABO�OM');
  w:= GetFirstWord(s, ' ');
  s:= RemoveLeftStr(s, w);
  if (curTank <> nil) and (Length(s) > 0) then
      curTank.manufacturer:= s;
end;

procedure TFileParser.ParseExtRepairInfo(str: string; rep_type: integer);
  var s: string;
      date: TDateTime;
      res: boolean;
      cpos: integer;
      rep_place: string;
      rep_code: string;
begin
  if curTank = nil then exit;

  if rep_type = 0 then      // Heavy repair info
    s:= RemoveLeftStr(str, cSecExtHeavyInfo)
  else if rep_type = 1 then   // Railroad repair info
    s:= RemoveLeftStr(str, cSecExtRailroadInfo)
  else exit;

  res:= ParseDate(s, date);

  if Pos('(0)', s) > 0 then
    s:= RemoveToStr(s, '(0)')
  else s:= RemoveToStr(s, 'HA');

  cpos:= Pos(' B ', s);
  if cpos > 0 then
    begin
      rep_place:= Trim(MidStr(s, 1, cpos));
      if Length(rep_place) > 0 then
        rep_place:= ' (' + rep_place + ')';   // �.�.
      s:= RemoveToStr(s, ' B ');
    end;
  rep_code:= GetFirstWord(s, ' ');   // code
  rep_place:= RemoveToStr(s, ' - ') + rep_place;

  if rep_type = 0 then
    begin
      if (Length(curTank.heavy_repair_date) = 0) and res
      then
        begin
          curTank.heavy_repair_date:= DateToStr(date);
          curTank.check_heavy_date:= curTank.heavy_repair_date;
        end;

      if (Length(curTank.heavy_repair_code) = 0) and
         (Length(rep_code) > 0)
      then
          curTank.heavy_repair_code:= rep_code;

      if (Length(curTank.heavy_repair_place) = 0) and
         (Length(rep_place) > 0)
      then
        begin
          curTank.heavy_repair_place:= rep_place;
          curTank.check_heavy_place:= rep_place;
        end;
    end
  else if rep_type = 1 then
    begin
      if (Length(curTank.railroad_repair_date) = 0) and res
      then
        begin
          curTank.railroad_repair_date:= DateToStr(date);
          curTank.check_railroad_date:= curTank.railroad_repair_date;
        end;

      if (Length(curTank.railroad_repair_code) = 0) and
         (Length(rep_code) > 0)
      then
          curTank.railroad_repair_code:= rep_code;
        
      if (Length(curTank.railroad_repair_place) = 0) and
         (Length(rep_place) > 0)
      then
        begin
          curTank.railroad_repair_place:= rep_place;
          curTank.check_railroad_place:= rep_place;
        end;
    end;
end;

//------------------------------------------------------------------------------

procedure TFileParser.ParseLine(str: string);
begin
  case curSec of

    // ����� ���������
    secGeneral: begin
        if StrBeginOn(str, cListTankNums) then
            // ������ ������� �� ������ ������ �������, "�����(�)"
            ParseListTankNums(str)
        else if StrBeginOn(str, cSecTechPasspBeg) then
            curSec:= secTechPassp
        else if StrBeginOn(str, cSecExtSep) then
            begin
              ParseExtTankNum(str);
              curSec:= secTechPasspExt;
            end;
      end;

    // ������ ����.�������� ������
    // ���������� ����� cSecTechPasspBeg � cSecTechPasspEnd
    secTechPassp: begin
        if curSec = secTechPassp then
          begin
            if StrBeginOn(str, cSecTechPasspEnd) then
                begin
                  curSec:= secGeneral;
                  curTank:= nil;
                end
            else if StrBeginOn(str, cSecTechPasspTitle) then
                ParseTechPasspTitle(str)
            else if StrBeginOn(str, cSecTechPasspOwner) then
                ParseTechPasspOwner(str)
            else if StrBeginOn(str, cSecTechPasspCompany) then
                ParseTechPasspCompany(str)
            else if StrBeginOn(str, cSecTechPasspManufacturer) then
                ParseTechPasspManufacturer(str)
            else if StrBeginOn(str, cSecTechPasspRepairs) then
                curSec:= secTechPasspRepairs;
          end
      end;

    // �������� � �������� � ����.�������� ������
    secTechPasspRepairs: begin
        if StrBeginOn(str, cSecTechPasspHeavy) then
          begin
            curSec:= secTechPasspHeavy;
            ParseHeavyDate(str);
          end
        else if StrBeginOn(str, cSecTechPasspRailroad) then
          begin
            curSec:= secTechPasspRailroad;
            ParseRailroadDate(str);
          end
        else if StrBeginOn(str, cSecSeparator) then
            curSec:= secTechPassp
        else if StrBeginOn(str, cSecTechPasspEnd) then
            curSec:= secTechPassp;
      end;

    //�������� � ���.������� � ����.��������
    secTechPasspHeavy: begin
        ParseHeavyPlace(str);
        curSec:= secTechPasspRepairs;
      end;

    //�������� � ���.������� � ����.��������
    secTechPasspRailroad: begin
        ParseRailroadPlace(str);
        curSec:= secTechPasspRepairs;
      end;

    // ���.��������, ����� ���� ���������
    secTechPasspExt: begin
        if StrBeginOn(str, cSecExtNextRepair) then
          begin
            ParseExtNextRepair(str);
            curSec:= secGeneral;
          end
        else if StrBeginOn(str, cSecTechPasspCompany) then
            ParseTechPasspCompany(str)
        else if StrBeginOn(str, cSecExtFabricate) then
            ParseExtFabricate(str)
        else if StrBeginOn(str, cSecExtHeavyInfo) then
            ParseExtRepairInfo(str, 0)
        else if StrBeginOn(str, cSecExtRailroadInfo) then
            ParseExtRepairInfo(str, 1);
      end;

    else
      raise Exception.Create('�������� ������ (curSec)');
  end;

end;

procedure TFileParser.ClearData;
  var i: integer;
      obj: TObject;
begin
  curSec:= secGeneral;
  curTank:= nil;

  for i:= 0 to tanks.Count-1 do
    begin
      obj:= tanks.Objects[i];
      if obj <> nil then
        obj.Free;
    end;
  tanks.Clear;
end;

end.
