unit f_Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, XPMan, u_Parser, f_Log, Mask;

const
  cols_count = 22;

type
  TfMain = class(TForm)
    edFile: TEdit;
    Label1: TLabel;
    btSel: TButton;
    btExit: TButton;
    btSave: TButton;
    XPManifest1: TXPManifest;
    dlgOpen: TOpenDialog;
    dlgSave: TSaveDialog;
    grid: TDrawGrid;
    procedure btSelClick(Sender: TObject);
    procedure btExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btSaveClick(Sender: TObject);
    procedure gridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure gridGetEditText(Sender: TObject; ACol, ARow: Integer;
      var Value: String);
    procedure gridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure gridSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: String);
    procedure gridClick(Sender: TObject);
    procedure gridGetEditMask(Sender: TObject; ACol, ARow: Integer;
      var Value: String);
  private
    { Private declarations }
    parser: TFileParser;
    selCol, selRow: integer;

    function GetTankListItem(ACol: integer; ARow: integer): string;
    procedure SetTankListItemValue(ACol: integer; ARow: integer; Value: string);
    function GetHeaderItem(ACol: integer): string;
    function GetColWidth(ACol: integer): integer;
    function CanItemEdit(ACol: integer): boolean;
    procedure SaveResults(fileName: string);
    procedure InitGrid;
  public
    { Public declarations }
  end;

var
  fMain: TfMain;

implementation

{$R *.dfm}

procedure TfMain.btSelClick(Sender: TObject);
  var fileName: string;
      res_cnt: integer;
begin
  try
    if dlgOpen.Execute then
      begin
        fileName:= dlgOpen.FileName;
        edFile.Text:= fileName;
        res_cnt:= parser.StartParse(fileName);
        if res_cnt > 0 then
          begin
            grid.RowCount:= res_cnt + 1;
            grid.SetFocus;
          end
        else
          grid.RowCount:= 2;

        btSave.Enabled:= res_cnt > 0;
      end;
  except
    on e: Exception do ShowMessage(e.Message);
  end;
  grid.Invalidate;
end;

procedure TfMain.btExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfMain.FormCreate(Sender: TObject);
begin
  try
    parser:= TFileParser.Create;
    InitGrid;
  except
    on e: Exception do ShowMessage(e.Message);
  end;
end;

procedure TfMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if parser <> nil then
    parser.Free;
end;

procedure TfMain.btSaveClick(Sender: TObject);
  var fileName: string;
begin
  try
    if (parser.TankList.Count > 0) and (dlgSave.Execute) then
      begin
        fileName:= dlgSave.FileName;
        SaveResults(fileName);
      end;
  except
    on e: Exception do ShowMessage(e.Message);
  end
end;

function TfMain.GetTankListItem(ACol: integer; ARow: integer): string;
  var res: string;
      tank: TTank;
begin
  res:= '';
  
  if (ACol >= 0) and (ACol < cols_count) and
     (ARow >= 0) and (ARow < parser.TankList.Count)
  then
    begin
      tank:= TTank(parser.TankList.Objects[ARow]);
      if tank <> nil then
        case ACol of
          0: res:= IntToStr(ARow+1);
          1: res:= tank.num;
          2: res:= tank.fabrication_date;
          3: res:= tank.manufacturer;
          4: res:= tank.owner;
          5: res:= '';  // ��� ��������� (tank.owner_code) ������� �� ����� ��������;
          6: res:= tank.rent_date;
          7: res:= tank.heavy_repair_date;
          8: res:= tank.heavy_repair_place;
          9: res:= tank.heavy_repair_code;
          10: res:= tank.check_heavy_date;
          11: res:= tank.check_heavy_place;
          12: res:= tank.next_heavy_date;
          13: res:= tank.railroad_repair_date;
          14: res:= tank.railroad_repair_place;
          15: res:= tank.railroad_repair_code;
          16: res:= tank.check_railroad_date;
          17: res:= tank.check_railroad_place;
          18: res:= tank.next_railroad_date;
          19: res:= '';
          20: res:= tank.tech_obslug_date;
          21: res:= tank.tech_obslug_place;
        end;
    end;

  Result:= res;
end;

procedure TfMain.SetTankListItemValue(ACol: integer; ARow: integer; Value: string);
  var tank: TTank;
begin
  if (ACol >= 0) and (ACol < cols_count) and
     (ARow >= 0) and (ARow < parser.TankList.Count) and (CanItemEdit(ACol))
  then
    begin
      tank:= TTank(parser.TankList.Objects[ARow]);
      if tank <> nil then
        case ACol of
          20: tank.tech_obslug_date:= Value;
          21: tank.tech_obslug_place:= Value;
        else
          raise Exception.Create('�� ��������� ������ �������� ��� ������ ������.');
        end;
    end;
end;

function TfMain.GetHeaderItem(ACol: integer): string;
  const headers: array [0..21] of string = (
          '� �/�',                                               // 0
          '� ��������',                                          // 1
          '���� ���������',                                      // 2
          '����� ���������',                                     // 3
          '��������',                                            // 4
          '����� ��������',                                      // 5 
          '���� ��������� ������',
          '���� ���������� ��',
          '����� ���������� ��',
          '�������� ��� ����������� ���������� ��',
          '���� ������������������� ����� ��',
          '����� ������������������� ����� ��',
          '���� ���������� ��',
          '���� ���������� ��',
          '����� ���������� ��',
          '�������� ��� ����������� ���������� ��',
          '���� ������������������� ����� ��',
          '����� ������������������� ����� ��',
          '���� ���������� ��',
          '��� ��������',
          '���� ���������������',                                // 20
          '����� ���������������'                                // 21
    );
  var res: string;
begin
  res:= '';
  if ACol <= High(headers) then
    res:= headers[ACol];

  Result:= res;
end;

function TfMain.GetColWidth(ACol: integer): integer;
  const col_width: array [0..21] of integer = (
            32,	  // 0
            62,	  // 1
            64,	  // 2
            160,	// 3
            139,	// 4
            64,	  // 5
            64,	  // 6
            67,	  // 7
            175,	// 8
            84,	  // 9
            118,	// 10
            118,	// 11
            85,	  // 12
            71,	  // 13
            148,	// 14
            86,	  // 15
            116,	// 16
            120,	// 17
            87,	  // 18
            50,   // 19
            95,   // 20
            120   // 21
        );

  var res: integer;
begin
  res:= 64;
  if ACol <= High(col_width) then
    res:= col_width[ACol];

  Result:= res;
end;

function TfMain.CanItemEdit(ACol: integer): boolean;
  const col_editable: array [0..21] of integer = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1);
  var res: boolean;
begin
  res:= false;
  if ACol <= High(col_editable) then
    res:= col_editable[ACol] > 0;

  Result:= res;
end;

procedure TfMain.InitGrid;
  var i: integer;
begin
  grid.ColCount:= cols_count;
  grid.RowHeights[0]:= 50;
  for i:=0 to grid.ColCount-1 do
    grid.ColWidths[i]:= GetColWidth(i);
end;

procedure TfMain.gridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);

  var val: string;
      flags: integer;
begin
  if ARow = 0 then
    begin
      flags:= DT_CENTER or DT_VCENTER or DT_WORDBREAK;
      val:= GetHeaderItem(Acol)
    end
  else
    begin
      flags:= DT_CENTER or DT_VCENTER or DT_WORDBREAK;
      val:= GetTankListItem(ACol, ARow-1);
    end;

  if (ARow > 0) and (CanItemEdit(ACol)) then
    begin
      grid.Canvas.Brush.Color:= clMoneyGreen;
      grid.Canvas.FillRect(Rect);
    end;

  Inc(Rect.Left, 2);
  Inc(Rect.Top, 2);
  Dec(Rect.Right, 2);

  DrawText(grid.Canvas.Handle, PChar(val), Length(val), Rect, flags);
end;

procedure TfMain.SaveResults(fileName: string);
  var hFile: TextFile;
      i, j: integer;
      str: string;
begin
  try
    AssignFile(hFile, fileName);
    Rewrite(hFile);

    for i:=0 to parser.TankList.Count do
      begin
        for j:= 1 to cols_count-1 do
          begin
            if j > 1 then str:= str + ';';

            if i = 0 then
                str:= str + GetHeaderItem(j)
            else 
                str:= str + GetTankListItem(j, i-1);
          end;

        Log(str);
        Writeln(hFile, str);
        str:= '';
      end;

    CloseFile(hFile);
  except
    on e: Exception do ShowMessage(e.Message);
  end;
end;

procedure TfMain.gridGetEditText(Sender: TObject; ACol, ARow: Integer;
  var Value: String);
begin
  if (not CanItemEdit(selCol)) then
    grid.Options:= grid.Options - [goEditing];

  Value:= GetTankListItem(ACol, ARow-1);
end;

procedure TfMain.gridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  selCol:= ACol;
  selRow:= ARow;
end;

procedure TfMain.gridSetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: String);
begin
  if Value <> '  .  .    ' then
    SetTankListItemValue(ACol, ARow-1, Value);
    
  grid.Options:= grid.Options - [goEditing];
end;

procedure TfMain.gridClick(Sender: TObject);
begin
  if (CanItemEdit(selCol)) then
    grid.Options:= grid.Options + [goEditing];
end;

procedure TfMain.gridGetEditMask(Sender: TObject; ACol, ARow: Integer;
  var Value: String);
begin
  if ACol = 20 then Value:= '!99/99/0000;1;_';
end;

end.
