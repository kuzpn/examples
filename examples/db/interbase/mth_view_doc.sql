create procedure mth_view_doc (id integer)
   returns (
	base_url varchar(250),
	doc_url  varchar(250)
)
as
	declare id_obj_type integer;
begin
	select id_obj_type
	from obj_type
	where id_obj = :id
	into :id_obj_type;

	if :id_obj_type = 8 then --budget
	        
		select doc.doc_url, url.url_base
		from obj
		inner join obj_type tp on tp.id_obj_type = obj.id_obj_type
		inner join document doc on doc.id_document = obj.id_obj
		inner join budget bd on bd.id_budget = obj.id_obj
		left join document_url url on url.id_doc_type = tp.id_obj_type and 
		   url.id_department = bd.id_department
		where id_obj = :id
		into :doc_url, :base_url

	else if :id_obj_type = 10 then  -- contract

	else if :id_obj_type = 12 then  -- project

		select doc.doc_url, url.url_base
		from obj
		inner join obj_type tp on tp.id_obj_type = obj.id_obj_type
		inner join document doc on doc.id_document = obj.id_obj
		inner join project pr on pr.id_project = obj.id_obj
		left join document_url url on url.id_doc_type = tp.id_obj_type and 
		   url.id_department = pr.id_department
		where id_obj = :id
		into :doc_url, :base_url

	else if :id_obj_type = 15 then  -- other_doc

		select doc.doc_url, url.url_base
		from obj
		inner join obj_type tp on tp.id_obj_type = obj.id_obj_type
		inner join document doc on doc.id_document = obj.id_obj
		inner join other_doc bd on bd.id_other_doc = obj.id_obj
		inner join other_doc_type otp on otp.id_other_doc_type = bd.id_other_doc_type
		left join document_url url on url.id_doc_type = tp.id_obj_type and 
		   url.id_department = bd.id_department and
		   url.comment = otp.name
		where id_obj = :id
		into :doc_url, :base_url
end