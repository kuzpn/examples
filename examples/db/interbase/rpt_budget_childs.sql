CREATE PROCEDURE RPT_BUDGET_CHILDS (
  ID_BUDGET_ROOT INTEGER,
  ID_PERIOD_TYPE INTEGER
) RETURNS (
  ID_BUDGET_CHILD INTEGER,
  NPP INTEGER
) AS  
	declare variable id_type integer;
begin
		for
			select b.id_budget, t.id_period_type, p.npp
			from budget b
				inner join period p on p.id_period = b.id_period
				inner join period_type t on 
							t.id_period_type = p.id_period_type
			where b.id_parent_budget = :id_budget_root
			into :id_budget_child, :id_type, :npp
		do
			if ( :id_type = :id_period_type ) then
			begin
			for
				select id_budget_child
				from g_all_budget_child(:id_budget_child)
                                into :id_budget_child
			do suspend;
		        end 
			else select id_budget_child, npp
				from rpt_budget_childs(:id_budget_child, 
							:id_period_type)
				into :id_budget_child, :npp;
end
