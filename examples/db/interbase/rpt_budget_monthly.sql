CREATE PROCEDURE RPT_BUDGET_MONTHLY (
  ID_BUDGET INTEGER
) RETURNS (
  ID_BUDGET_ROW INTEGER,
  PERIOD_NPP INTEGER,
  DEPARTMENT VARCHAR(250),
  NAME VARCHAR(120),
  ITEM_PARENT VARCHAR(80),
  ITEM_NAME VARCHAR(80),
  ROW_SUM NUMERIC(15, 2),
  CONTRAGENT_NAME VARCHAR(250),
  CONTRACT_N VARCHAR(20),
  CONTRACT_DATE TIMESTAMP,
  PERIOD_PAYMENT VARCHAR(20),
  DATE_PAYMENT TIMESTAMP,
  COMMENT VARCHAR(255)
) AS    
	declare id_parent integer;
begin
  for 
     select r.id_budget_row, p.npp, d.name, r.name, i.id_parent_budget_item, i.name,
		a.name_full, r.row_sum, c.doc_n_str, c.doc_date, NULL, NULL, r.comment
     from budget_row r
     inner join budget_item i on i.id_budget_item = r.id_budget_item
     inner join budget b on b.id_budget = r.id_budget
     inner join department d on d.id_department = b.id_department
     inner join period p on p.id_period = b.id_period
     left join contract c on c.id_contract = r.id_contract
     left join contragent a on a.id_contragent = c.id_contragent
     where r.id_budget = :id_budget
     into :id_budget_row, :period_npp, :department, :name, :id_parent, :item_name, :contragent_name, :row_sum, 
          :contract_n, :contract_date, :period_payment, :date_payment, :comment
  do
     begin
         
        item_parent = :item_name;
	while (:id_parent != 1 and not :id_parent is null) do
	begin
		select id_parent_budget_item, name
		from budget_item
		where id_budget_item = :id_parent
		into :id_parent, :item_parent;
	end

	suspend;
     end
end
