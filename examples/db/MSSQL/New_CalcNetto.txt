/* test get_qpassport_density
begin
  declare
	@density		float,
	@sample_temperature	float,
	@id_rwquality_passport	int,
	@val			char(32)

  select @id_rwquality_passport = 15702

  exec get_qpassport_density @id_rwquality_passport, @density OUTPUT, @sample_temperature OUTPUT

  select @val = '@density = ' + str(@density, 8, 5)
  print @val
  select @val = '@sample_temperature = ' + str(@sample_temperature, 8, 5)
  print @val
end
*/

-- kpn 30.10.2008
-- ���������� ��������� �� �������� ��������
-- ���������� �� get_density_for_group
--------------------------------------------
create procedure get_qpassport_density (
	@id_rwquality_passport	int,
	@density		float OUTPUT,
	@sample_temperature	float OUTPUT
)
as
begin
  declare @dens_t20		float,
	  @dens_test_temp	float,
	  @msg_err		varchar(100)

  select
	@density = NULL,
	@sample_temperature = NULL

  select @dens_test_temp = 20

  -- ��������� ��� 20 ��������
  if exists (
    select qpr.id_rwquality_passport, prm.param_value
    from rwpassport_row qpr
      inner join phisical_parameter pp on pp.id_phisical_parameter = qpr.id_phisical_parameter 
      inner join phisical_parameter_prm prm on prm.id_phisical_parameter = pp.id_phisical_parameter
		and prm.param_value = @dens_test_temp
    where 
	  qpr.id_rwquality_passport = @id_rwquality_passport
      and pp.id_phis_param_type = 2
  )
  begin

    select @dens_t20 = min( qpr.factvalue_f )
    from rwpassport_row qpr
      inner join phisical_parameter pp on pp.id_phisical_parameter = qpr.id_phisical_parameter 
      inner join phisical_parameter_prm prm on prm.id_phisical_parameter = pp.id_phisical_parameter
		and prm.param_value = @dens_test_temp
    where 
	  qpr.id_rwquality_passport = @id_rwquality_passport
      and pp.id_phis_param_type = 2


    if @dens_t20 is null
    begin
      select @msg_err = '�� ��������� ��������� ��� 20 �������� � �������� ��������.'
      goto error
    end

  end		-- if exists(��� 20 ����.)  
  else  	
  -- ���� ��� ��������� ��� 20 ��������, ���� ��� 15
  begin

    select @dens_test_temp = 15

    select @dens_t20 = min( qpr.factvalue_f )
    from rwpassport_row qpr
      inner join phisical_parameter pp on pp.id_phisical_parameter = qpr.id_phisical_parameter 
      inner join phisical_parameter_prm prm on prm.id_phisical_parameter = pp.id_phisical_parameter
		and prm.param_value = @dens_test_temp
    where 
	  qpr.id_rwquality_passport = @id_rwquality_passport
      and pp.id_phis_param_type = 2

    if @dens_t20 is null
    begin
      select @msg_err = '�� ��������� ��������� ��� 15 �������� � �������� ��������.'
      goto error
    end

  end		-- if exists(��� 20 ����)  else

  if @dens_t20 > 100 select @dens_t20 = @dens_t20 / 1000

  select 
    @density = @dens_t20,
    @sample_temperature = @dens_test_temp

  return 0

error:
  raiserror( @msg_err, 16, -1 )
  return -20

end

GRANT  EXECUTE  ON dbo.get_qpassport_density TO rwoperator
GO



/* test get_density_for_group
begin
  declare
	@density_t20		float,
	@density		float,
	@id_rwtank_group	int,
	@val			char(32)

  select @id_rwtank_group = 75567

  exec get_density_for_group @id_rwtank_group, @density_t20 OUTPUT, @density OUTPUT

  select @val = '@density_t20 = ' + str(@density_t20, 8, 5)
  print @val
  select @val = '@density = ' + str(@density, 8, 5)
  print @val
end
*/

-- kpn 30.10.2008
-- � ������ ��������� �� ���������� ����������� % ���������� ����� ��� ������,
-- �.�. � ����-� sp_CalcVolumeTank � sp_rwtank_group_set_density ��� ������������,
-- �� � ����������� �������� ����� �� �����������
----------------------------------------------------------------------------------
create procedure get_density_for_group (
	@id_rwtank_group	int,
	@density_t20		float OUTPUT,
	@density		float OUTPUT
)
as
begin
  declare
	@id_rwquality_passport	int,
	@temperature		float,
	@dens_test_temp		float,
	@adjustment		float,
	@dens			float,
	@dens_t20		float,
	@id_prod_type		int,
	@res			int,
	@msg_err		varchar(100)

  select @density = 0, @density_t20 = 0,
	@dens_test_temp = 20

  select @temperature = temperature,
	 @dens_t20 = density_t20,	-- ��������� ��������� ���.����������
	 @id_rwquality_passport = id_rwquality_passport
  from rwtank_group
  where id_rwtank_group = @id_rwtank_group

  if @temperature is null 
  begin
    select @msg_err = '�� ��������� ����������� � ������ �������.'
    goto error
  end

  if @id_rwquality_passport is null
  begin
    select @msg_err = '�� ������ ������� �������� ��� ������ �������.'
    goto error
  end

-- ���������� ��� ��������.
  select @id_prod_type = p.id_prod_type
  from rwtank_group tg
    inner join product p on p.id_product = tg.id_rwtank_group
  where tg.id_rwtank_group = @id_rwtank_group

  if @id_prod_type = 49		-- ����������� ��������� ��� ������
  begin

    -- ����� �� sp_rwtank_group_set_density
    select @density = NULL		-- 0.982 - ((@t_n - 100) * 0.006 /10) 

    -- ��. sp_CalcVolumeTank
    --   ����� ������ ������������ �� ���������� -- kvp 22.02.2007 
    --   select @density_t = 0.982 - (( @tank_product_T - 100 ) * 0.006 /10 )

  end		-- if @id_prod_type = 49 ����������� ��������� ��� ������
  else		
  begin

    if @dens_t20 is null		-- ���� ��������� �� ������� � ������, �� ����� � �� ��
    begin

      exec @res = get_qpassport_density @id_rwquality_passport, @dens_t20 OUTPUT, @dens_test_temp OUTPUT
      if @res <> 0 return @res
	
      if @dens_t20 is null
      begin
        select @msg_err = '�� �������� ������� �������� ��� ��������� �������� � �������� ��������.'
        goto error
      end
    end		-- if dens_t20 is null

    select @adjustment = min( density_adjustment )
    from rwtemperature_density
    where 
          @dens_t20 >= density1
      and @dens_t20 < round( density2, 2)

   if @adjustment is null
   begin
     select @msg_err = '��������� �������� � �������� �������� ������� �� ����� �������� ������������ � ������� �����������-���������.'
     goto error
   end

   select @dens = ( @dens_t20 + ( @dens_test_temp - @temperature ) * @adjustment )

  end		-- if @id_prod_type = 49 else


-- ������� �����������
  select 
	@density = @dens,
	@density_t20 = @dens_t20

  return 0

error:
  raiserror( @msg_err, 16, -1 )
  return -20

end

GRANT  EXECUTE  ON dbo.get_density_for_group TO rwoperator
GO




/* test calc_rwtank_netweight
begin
  declare
	@id_rwtank_group	int,
	@id_rwtank_type		int,
	@rwtank_level		float,
	@netweight		float,
	@val			char(32)

  select @id_rwtank_group = 75567,
         @rwtank_level = 2.2

  select @id_rwtank_type = id_rwtank_type
  from rwtank_type
  where container_code = '60'

  if @id_rwtank_type is not null
  begin
    exec calc_rwtank_netweight @id_rwtank_group, @id_rwtank_type, @rwtank_level, @netweight OUTPUT

    select @val = '@netweight = ' + str(@netweight, 8, 5)
    print @val
  end
  else
    print '�� ������ ���������'
end
*/

-- kpn 30.10.2008
-- ������ sp_CalcVolumeTank
--------------------------------------------------
create procedure calc_rwtank_netweight (
	@id_rwtank_group	int,
	@id_rwtank_type		int,
	@rwtank_level		float,
	@netweight		float OUTPUT
)
as
begin
  declare 
	@id_rwtank_kind	int,
	@dens_t20	float,
	@density	float,
	@vlaga		float,
	@tank_volume	float,
	@netto		float,
	@res		int,
	@msg_err	varchar(100)

  select 
	@netweight = 0,
	@netto = 0

  -- �������� ����������
  if not exists( 
	select * 
	from rwtank_group
	where id_rwtank_group = @id_rwtank_group )
  begin
	select @msg_err = '������ ������� ������ �������. (ID: ' + convert( varchar, @id_rwtank_group ) + ')'
	goto error
  end


  select @id_rwtank_kind = id_rwtank_kind
  from rwtank_type
  where id_rwtank_type = @id_rwtank_type


  if( @id_rwtank_kind = 1 )	-- ������ ��������
  begin

    exec @res = get_density_for_group @id_rwtank_group, @dens_t20 OUTPUT, @density OUTPUT
    if @res <> 0 return @res

    -- ����������� ������ ��������
    exec g_rwtank_volume_2007 @id_rwtank_type, @rwtank_level, @tank_volume OUTPUT
    if( @tank_volume is null ) begin
      select @msg_err = '������ ����������� ������ ��������. g_rwtank_volume_2007'
      goto error
    end

/*	-- � �������� � sp_CalcVolumeTank ����� �� ���������
    select @vlaga = 0

    if( @id_prod_type = 44 )	-- ��� ������ ���������� % ���������� �����
    begin
      

      select @vlaga = (
        select min( qpr.factvalue_f ) / 100
        from rwtank_group tg
          inner join rwqpassport_row qpr on 
                qpr.id_rwquality_passport = tg.id_rwquality_passport and
                qpr.id_product = tg.id_product
          inner join phisical_parameter pp on pp.id_phisical_parameter = qpr.id_phisical_parameter
        where tg.id_rwtank_group = @id_rwtank_group and
              pp.id_phis_param_type = 4
      )

      if @vlaga is null 
      begin
        select @msg_err = '�� �������� ��������: "���������� �����" � �������� ��������.'
	goto error
      end

    end			-- if( @id_prod_type = 44 ) ����������� % ���������� �����
*/


    -- ��������� NETTO
    select @netto = coalesce( @tank_volume, 0 ) * coalesce( @density, 0 )
    select @netto = round( @netto, 3 )


    if( @netto is null ) begin
      select @msg_err = '������ ����������� ���� ����� � ��������� calc_rwtank_netweight'
      goto error
    end

  end	-- if( @id_rwtank_kind = 1 )	������ ��������

  select @netweight = @netto 	-- ���������

  return 0

error:
  raiserror (@msg_err, 16, -1)
  return -20

end

GRANT  EXECUTE  ON dbo.calc_rwtank_netweight TO rwoperator
GO



/* test calc_rwtank_netweight & sp_CalcVolumeTank */
begin
  declare
	@id_rwtank_group	int,
	@id_rwtank_type		int,
	@rwtank_level		float,
	@netweight_old		float,
	@netweight_new		float,
	@val			varchar(255)

  declare cur cursor for
	select max(id_rwtank_group) as id_rwtank_group
	from rwtank_group
	group by id_product
	order by 1

  select @rwtank_level = 2.2

  select @id_rwtank_type = id_rwtank_type
  from rwtank_type
  where container_code = '60'

  if @id_rwtank_type is null
  begin
      raiserror( '�� ������ ���������', -16, 1 )
  end

  open cur
  fetch next from cur into @id_rwtank_group

  while @@fetch_status = 0
  begin

      exec calc_rwtank_netweight @id_rwtank_group, @id_rwtank_type, @rwtank_level, @netweight_new OUTPUT 
      exec sp_CalcVolumeTank @id_rwtank_group, @id_rwtank_type, @rwtank_level, @netweight_old OUTPUT, 1, 0

      if @netweight_new = @netweight_old 
	select @val = '+' 
      else select @val = '-'

      select @val = @val + '  @id_rwtank_group = ' + str(@id_rwtank_group)
      select @val = @val + '  @netweight_new = ' + str(@netweight_new, 8, 5)
      select @val = @val + '  @netweight_old = ' + str(@netweight_old, 8, 5)
      print @val

    fetch next from cur into @id_rwtank_group
  end
  close cur
  deallocate cur

end
/* */



/* test sp_CalcVolumeTank;2 */
begin
  declare 
	@id_rwtank_group	int,
	@id_rwtank_type		int,
	@rwtank_level		float,
	@netweight_my		float,
	@netweight_cur		float,
	@val			varchar(255)

  declare cur cursor for
	select tg.id_rwtank_group, t.id_rwtank_type, t.rwtank_level, t.netweight
	from rwtank t
	  inner join rwtank_group tg on tg.id_rwtank_group = t.id_rwtank_group
	where tg.x_owner_time > convert(datetime, '25.07.2008 00:00:00', 104)
	order by tg.id_rwtank_group, t.npp 

  open cur
  fetch next from cur into @id_rwtank_group, @id_rwtank_type, @rwtank_level, @netweight_cur

  while @@fetch_status = 0
  begin

    exec sp_CalcVolumeTank;2
	@id_rwtank_group,
	@id_rwtank_type,
	@rwtank_level,
	@netweight_my OUTPUT, 1, 0

    if @netweight_cur <> @netweight_my
    begin
      select @val = '-'
      select @val = @val + '  @id_rwtank_group = ' + str(@id_rwtank_group)
      select @val = @val + '  @id_rwtank_type = ' + str(@id_rwtank_type)
      select @val = @val + '  @rwtank_level = ' + str(@rwtank_level)
      select @val = @val + '  @netweight_cur = ' + str(@netweight_cur, 8, 5)
      select @val = @val + '  @netweight_my = ' + str(@netweight_my, 8, 5)
      print @val
    end

    fetch next from cur into @id_rwtank_group, @id_rwtank_type, @rwtank_level, @netweight_cur

  end
  close cur
  deallocate cur

end



-- kpn 30.10.2008
--------------------------------------------
create procedure sp_CalcVolumeTank;2 (
	@id_rwtank_group int,
	@id_rwtank_type int,
	@rwtank_level float,
	@netweight float OUTPUT,
	@raise_exceptions tinyint = 1,
	@update_density tinyint = 0
)
as
begin
  declare @res int

  select @netweight = 0

  exec @res = calc_rwtank_netweight @id_rwtank_group, @id_rwtank_type, @rwtank_level, @netweight OUTPUT
  return @res
end




-- kpn 30.10.2008
----------------------------------------------
create procedure sp_rwtank_group_set_density;2 @id_rwtank_group int , @density float OUTPUT, 
				@update_rwtank_density int = 0, @raise_exceptions int = 0 
as
begin
  declare
	@dens_t20	float,
	@res		int

  select @dens_t20 = density_t20
  from rwtank_group
  where id_rwtank_group = @id_rwtank_group

  if @dens_t20 is not null
  begin

    select @density = 0

    exec @res = get_density_for_group @id_rwtank_group, @dens_t20 OUTPUT, @density OUTPUT
    if @res <> 0 return @res

    if ( @density is not null ) and ( @density > 0) 
	and ( @update_rwtank_density = 1 )
    begin
      update rwtank_group
      set density = @density
      where id_rwtank_group = @id_rwtank_group
    end

  end
  else
    exec sp_rwtank_group_set_density;1 @id_rwtank_group, @density, 
		@update_rwtank_density, @raise_exceptions

end
