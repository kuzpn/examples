select p.id, u.uid, o.name, o.type, u.name, 
	case sum(
		case p.action
			when 193 then 1
			when 195 then 2
			when 196 then 4
			when 197 then 8
			else 0
		end
	)
		when 0 then '----'
		when 1 then 'S---'
		when 2 then '-U--'
		when 3 then 'SU--'
		when 4 then '--I-'
		when 5 then 'S-I-'
		when 6 then '-UI-'
		when 7 then 'SUI-'
		when 8 then '---D'
		when 9 then 'S--D'
		when 10 then '-U-D'
		when 11 then 'SU-D'
		when 12 then '--ID'
		when 13 then 'S-ID'
		when 14 then '-UID'
		when 15 then 'SUID'
		else ''
	end as rights
from sysprotects p
  inner join sysobjects o on o.id = p.id
  inner join sysusers u on u.uid = p.uid
where o.type = 'U' and
--	o.name like ('%nakl%') and
	u.name = 'transport'
group by p.id, u.uid, o.name, o.type, u.name
order by o.type desc, o.name



select p.id, u.uid, o.name, o.type, u.name
from sysprotects p
  inner join sysobjects o on o.id = p.id
  inner join sysusers u on u.uid = p.uid
where (o.type = 'U' or o.type = 'P') and
--	o.name like ('%nakl%') and
	u.name = 'transport'
