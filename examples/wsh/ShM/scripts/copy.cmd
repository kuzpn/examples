@echo off

set SERVER=192.168.0.1
set USER=market
set PASS=market
set FTPCMD=cscript //NoLogo %~d0%~p0\ftp.wsf /server:%SERVER% /user:%USER% /pass:%PASS%

if defined SRC (
  set SRC=/src:%SRC%
) else (
  set SRC=
)

if defined DST (
  set DST=/dst:%DST%
) else (
  set DST=
)

if defined CONVSRC (
  set CONVSRC=/convSrc:%CONVSRC%
) else (
  set CONVSRC=
)

%FTPCMD% %SRC% %DST% %CONVSRC%
