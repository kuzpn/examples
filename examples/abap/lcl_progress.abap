" Возвращает строку прогрессбара для статусбара
"
CLASS lcl_progress DEFINITION CREATE PRIVATE.
  PUBLIC SECTION.
    TYPES: t_progress_type TYPE i.

    CONSTANTS:
      C_PROGRESS_NONE       TYPE t_progress_type VALUE 0,
      C_PROGRESS_RANDOM     TYPE t_progress_type VALUE 1,
      C_PROGRESS_DOTS       TYPE t_progress_type VALUE 2,
      C_PROGRESS_SPIN       TYPE t_progress_type VALUE 3,
      C_PROGRESS_ACCORDION  TYPE t_progress_type VALUE 4.

    CLASS-METHODS:
      create IMPORTING progress_type TYPE t_progress_type RETURNING VALUE(ref) TYPE REF TO lcl_progress.

    METHODS:
      next RETURNING VALUE(line) TYPE string,
      next_indicate IMPORTING msg TYPE string,
      stop_indicate,
      reset.

  PRIVATE SECTION.
    METHODS:
      constructor IMPORTING progress_type TYPE t_progress_type,
      get_random_progress RETURNING VALUE(res) TYPE t_progress_type,
      get_progress_dots RETURNING VALUE(res) TYPE string,
      get_progress_spin RETURNING VALUE(res) TYPE string,
      get_progress_accordion RETURNING VALUE(res) TYPE string.

   DATA:
      mv_type TYPE t_progress_type,
      mv_count TYPE i.
ENDCLASS.


CLASS lcl_progress IMPLEMENTATION.

METHOD create.
  " IMPORTING progress_type TYPE t_progress_type RETURNING VALUE(ref) TYPE REF TO lcl_progress.

  DATA: lo_ref TYPE REF TO lcl_progress.
  CREATE OBJECT lo_ref EXPORTING progress_type = progress_type.
  ref = lo_ref.
ENDMETHOD.

METHOD constructor.
  " IMPORTING progress_type TYPE t_progress_type

  IF progress_type = C_PROGRESS_RANDOM.
    mv_type = get_random_progress( ).
  ELSE.
    mv_type = progress_type.
  ENDIF.
  mv_count = 0.
ENDMETHOD.

METHOD next.
  " RETURNING VALUE(line) TYPE string

  IF mv_type = C_PROGRESS_NONE.
    CLEAR line.
    RETURN.
  ENDIF.

  DATA: lv_line TYPE string.
  ADD 1 TO mv_count.
  CASE mv_type.
    WHEN C_PROGRESS_DOTS.
      lv_line = get_progress_dots( ).
    WHEN C_PROGRESS_SPIN.
      lv_line = get_progress_spin( ).
    WHEN C_PROGRESS_ACCORDION.
      lv_line = get_progress_accordion( ).
  ENDCASE.
  line = lv_line.
ENDMETHOD.

METHOD next_indicate.
  " IMPORTING msg TYPE string

  DATA: lv_line TYPE string.
  lv_line = |{ msg }{ me->next( ) }|.
  cl_progress_indicator=>progress_indicate( EXPORTING i_text = lv_line i_output_immediately = 'X' ).
ENDMETHOD.

METHOD stop_indicate.
  cl_progress_indicator=>progress_indicate( EXPORTING i_text = ' ' i_output_immediately = 'X' ).
ENDMETHOD.

METHOD reset.
  mv_count = 0.
ENDMETHOD.

METHOD get_random_progress.
  " RETURNING VALUE(res) TYPE t_progress_type,

  DATA: lv_res LIKE res.
  CALL FUNCTION 'QF05_RANDOM_INTEGER'
    EXPORTING
      ran_int_max         = 4
      ran_int_min         = 2
    IMPORTING
      ran_int             = lv_res
    EXCEPTIONS
      invalid_input       = 1
      OTHERS              = 2.

  IF sy-subrc <> 0.
    lv_res = C_PROGRESS_NONE.
  ENDIF.
  res = lv_res.
ENDMETHOD.

METHOD get_progress_dots.
  CONSTANTS: dots TYPE c LENGTH 10 VALUE '..........'.
  DATA: len TYPE i.

  len = ( ( mv_count DIV 10 ) MOD 10 ) + 1.
  res = dots(len).
ENDMETHOD.

METHOD get_progress_spin.
  CONSTANTS: positions TYPE c LENGTH 8 VALUE '|/-\|/-\'.
  DATA: len TYPE i.

  len = ( mv_count DIV 10 ) MOD 8.
  res = positions+len(1).
ENDMETHOD.

METHOD get_progress_accordion.
  CONSTANTS: positions TYPE c LENGTH 10 VALUE '||||||||||'.
  DATA: len TYPE i,
        t TYPE i.

  t = mv_count DIV 10.
  len = ABS( ( ( t MOD 10 ) + 1 ) - 11 * ( t DIV 10 MOD 2 ) ).
  res = |[:{ positions(len) }:]|.
ENDMETHOD.

ENDCLASS.
