" Обработчик событий ALV-грида
"
CLASS lcl_scr0100_event_receiver DEFINITION.
  PUBLIC SECTION.
    METHODS:
      handle_data_changed
          FOR EVENT data_changed OF cl_gui_alv_grid
            IMPORTING er_data_changed,

      handle_on_f4
          FOR EVENT onf4 OF cl_gui_alv_grid
            IMPORTING
              e_fieldname
              e_fieldvalue
              es_row_no
              er_event_data
              et_bad_cells
              e_display,

     handle_change_finished
          FOR EVENT data_changed_finished OF cl_gui_alv_grid
            IMPORTING
              e_modified
              et_good_cells,

     handle_hotspot_click
          FOR EVENT hotspot_click OF cl_gui_alv_grid
            IMPORTING
              e_row_id
              e_column_id
              es_row_no.

  PRIVATE SECTION.
    METHODS:
      handle_hotspot_docid
            IMPORTING
              e_row_id TYPE lvc_s_row,
      save_corb_with_docid
            IMPORTING
              e_row_id TYPE lvc_s_row.
ENDCLASS.                    "lcl_scr0100_event_receiver DEFINITION



CLASS lcl_scr0100_event_receiver IMPLEMENTATION.

  METHOD handle_data_changed.
    DATA:
      ls_row TYPE lvc_s_moce,
      ls_mod TYPE lvc_s_modi,
      lv_timestampl TYPE timestampl.

    FIELD-SYMBOLS:
      <fs_corbp> LIKE LINE OF gt_corbp.

    LOOP AT er_data_changed->mt_deleted_rows INTO ls_row.
      READ TABLE gt_corbp ASSIGNING <fs_corbp> INDEX ls_row-row_id.
      IF sy-subrc = 0.
        IF <fs_corbp>-checkmark = 'X'.
          CONTINUE.
        ENDIF.

        IF <fs_corbp>-inserted <> 'X'.
          APPEND <fs_corbp> TO gt_corbp_deleted.
        ENDIF.
      ENDIF.
    ENDLOOP.

    LOOP AT er_data_changed->mt_inserted_rows INTO ls_row.
      er_data_changed->modify_cell( EXPORTING
          i_row_id = ls_row-row_id
          i_fieldname = 'GJAHR'
          i_value = p_gjahr ).

      GET TIME STAMP FIELD lv_timestampl.
      er_data_changed->modify_cell( EXPORTING
          i_row_id = ls_row-row_id
          i_fieldname = 'TIMESTAMPL'
          i_value = lv_timestampl ).

      er_data_changed->modify_cell( EXPORTING
          i_row_id = ls_row-row_id
          i_fieldname = 'INSERTED'
          i_value = 'X' ).

      er_data_changed->modify_cell( EXPORTING
          i_row_id = ls_row-row_id
          i_fieldname = 'CHECKMARK'
          i_value = ' ' ).
    ENDLOOP.

    LOOP AT er_data_changed->mt_mod_cells INTO ls_mod.
      READ TABLE gt_corbp ASSIGNING <fs_corbp> INDEX ls_mod-row_id.
      IF sy-subrc = 0.
        <fs_corbp>-updated = 'X'.
      ENDIF.
    ENDLOOP.
  ENDMETHOD.                    "handle_data_changed

  METHOD handle_on_f4.
    DATA:
      lv_tabname    TYPE dfies-tabname,
      lv_fieldname  TYPE dfies-fieldname,
      lt_values     TYPE TABLE OF ddshretval,
      ls_value      LIKE LINE OF lt_values.

    FIELD-SYMBOLS: <fs_data> TYPE lvc_t_modi.
    DATA: ls_modi LIKE LINE OF <fs_data>.

    IF e_fieldname = 'FICTR'.
      lv_tabname = 'FMFCTR'.
      lv_fieldname = 'FICTR'.
    ELSEIF e_fieldname = 'ZFINAZNPL'.
      lv_tabname = 'ZFIBABSR'.
      lv_fieldname = 'ZFINAZNPL'.
    ELSE.
      CLEAR er_event_data->m_event_handled.
      RETURN.
    ENDIF.

    CALL FUNCTION 'F4IF_FIELD_VALUE_REQUEST'
      EXPORTING
        tabname           = lv_tabname
        fieldname         = lv_fieldname
      TABLES
        return_tab        = lt_values
      EXCEPTIONS
        field_not_found   = 1
        no_help_for_field = 2
        inconsistent_help = 3
        no_values_found   = 4
        OTHERS            = 5.

    IF sy-subrc <> 0.
      MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
              WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    ENDIF.

    READ TABLE lt_values INTO ls_value WITH KEY fieldname = lv_fieldname.
    ASSIGN er_event_data->m_data->* TO <fs_data>.

    LOOP AT lt_values INTO ls_value.
      IF ls_value-fieldname = lv_fieldname OR ls_value-fieldname = 'ZBDDS'.
        ls_modi-row_id = es_row_no-row_id.
        ls_modi-fieldname = ls_value-fieldname.
        ls_modi-value = ls_value-fieldval.
        APPEND ls_modi TO <fs_data>.
      ENDIF.
    ENDLOOP.

    er_event_data->m_event_handled = 'X'.
  ENDMETHOD.                    "handle_on_f4

  METHOD handle_change_finished.
    DATA: lv_refresh TYPE abap_bool.
    FIELD-SYMBOLS:
      <fs_cell>   LIKE LINE OF et_good_cells,
      <fs_corbp>  LIKE LINE OF gt_corbp.

    LOOP AT et_good_cells ASSIGNING <fs_cell>.
      READ TABLE gt_corbp ASSIGNING <fs_corbp> INDEX <fs_cell>-row_id.
      IF sy-subrc <> 0. CONTINUE. ENDIF.

      CASE <fs_cell>-fieldname.
        WHEN 'ZBDDS'.
          PERFORM get_zbddst    USING <fs_corbp>-zbdds CHANGING <fs_corbp>-zbddst.
          PERFORM get_zbdds_up  USING <fs_corbp>-zbdds CHANGING <fs_corbp>-zbdds_up.
          PERFORM get_zbddst    USING <fs_corbp>-zbdds_up CHANGING <fs_corbp>-zbddst_up.
          PERFORM get_beschr    USING <fs_corbp>-fictr CHANGING <fs_corbp>-beschr.
          lv_refresh = abap_true.
        WHEN 'ZFINAZNPL'.
          PERFORM check_zbdds_zfinaznpl USING <fs_corbp>-zbdds <fs_corbp>-zfinaznpl.
          IF sy-subrc = 0.
            PERFORM get_prctr USING <fs_corbp>-zbdds <fs_corbp>-zfinaznpl CHANGING <fs_corbp>-prctr.
            lv_refresh = abap_true.
          ELSE.
*            MESSAGE 'Позиция с назн.платежа отсутствует в справочнике Назн.платежаАСБ' TYPE 'S' DISPLAY LIKE 'E'.
          ENDIF.
        WHEN 'SUMM'.
          PERFORM calc_remains CHANGING <fs_corbp>.
          lv_refresh = abap_true.
      ENDCASE.
    ENDLOOP.

    IF lv_refresh = abap_true.
      DATA: stable TYPE lvc_s_stbl VALUE 'XX'.
      go_grid->refresh_table_display( is_stable = stable ).
    ENDIF.
  ENDMETHOD.                    "handle_change_finished

  METHOD handle_hotspot_click.
    CASE e_column_id.
      WHEN 'DOCID'.
        handle_hotspot_docid( e_row_id ).
    ENDCASE.
  ENDMETHOD.

  METHOD handle_hotspot_docid.
    DATA:
      lv_res TYPE string,
      lv_ok  TYPE abap_bool.
    FIELD-SYMBOLS: <fs_row> LIKE LINE OF gt_corbp.

    READ TABLE gt_corbp ASSIGNING <fs_row> INDEX e_row_id-index.
    CHECK sy-subrc = 0.
    IF <fs_row>-docid IS INITIAL.
                                                        " Загрузить документ
      PERFORM confirm_and_save CHANGING lv_ok.
      IF lv_ok = abap_true.
        PERFORM upload_document CHANGING <fs_row>-docid.
        IF <fs_row>-docid IS NOT INITIAL.
          <fs_row>-updated = 'X'.
          save_corb_with_docid( e_row_id ).
          go_grid->refresh_table_display( ).
        ENDIF.
      ENDIF.
    ELSE.
                                                        " Открыть / Удалить
      PERFORM confirm_doc_action CHANGING lv_res.
      IF lv_res = '1'.      " Открыть
        PERFORM show_document USING <fs_row>-docid.

      ELSEIF lv_res = '2'.  " Удалить
        PERFORM confirm_and_save CHANGING lv_ok.
        IF lv_ok = abap_true.
          PERFORM delete_document CHANGING <fs_row>-docid.
          IF <fs_row>-docid IS INITIAL.
            <fs_row>-updated = 'X'.
            save_corb_with_docid( e_row_id ).
            go_grid->refresh_table_display( ).
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.
  ENDMETHOD.

  METHOD save_corb_with_docid.
    DATA: lv_ok TYPE abap_bool.

    PERFORM save_corbp_row USING e_row_id-index CHANGING lv_ok.
    IF lv_ok <> abap_true.
      MESSAGE 'Ошибка записи данных в БД' TYPE 'S' DISPLAY LIKE 'E'.
    ENDIF.
  ENDMETHOD.

ENDCLASS.