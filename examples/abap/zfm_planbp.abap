" INCLUDE ZFM_PLANBP_SSCR
"
SELECTION-SCREEN BEGIN OF BLOCK bl_basic WITH FRAME TITLE text-001.
PARAMETERS:
  p_bukrs TYPE bukrs DEFAULT '1000',
  p_gjahr TYPE gjahr DEFAULT sy-datum(4).

SELECT-OPTIONS:
  so_fictr FOR kblp-fistl,										" ЦО
  so_zbdds FOR fmci-zbdds.										" Статья БДДС
SELECTION-SCREEN END OF BLOCK bl_basic.


SELECTION-SCREEN BEGIN OF BLOCK bl_opts WITH FRAME TITLE text-002.
  PARAMETERS:
    p_view RADIOBUTTON GROUP grp1 USER-COMMAND grp1 DEFAULT 'X',	" Утвержденный план на год
    p_corr RADIOBUTTON GROUP grp1,											" Скорректированный план на год
    p_load RADIOBUTTON GROUP grp1.											" Загрузка данных

  SELECTION-SCREEN BEGIN OF LINE.
    SELECTION-SCREEN COMMENT 5(8) text-003.
    PARAMETERS:
      p_file TYPE tv_filename.												" Файл
  SELECTION-SCREEN END OF LINE.
SELECTION-SCREEN END OF BLOCK bl_opts.

SELECTION-SCREEN BEGIN OF BLOCK bl_var WITH FRAME TITLE text-004.
  PARAMETERS: p_var TYPE slis_vari. 				" Вариант
SELECTION-SCREEN END OF BLOCK bl_var.


INITIALIZATION.
  PERFORM selscr_init.

AT SELECTION-SCREEN OUTPUT.
  PERFORM selscr_pbo.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_file.
  PERFORM f4_select_file.

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_var.
  PERFORM alv_variant_f4.

START-OF-SELECTION.
  PERFORM start_of_selection.

"
" INCLUDE ZFM_PLANBP_FORMS
"......................
"......................
"
FORM f4_select_file.
  DATA: lt_files TYPE filetable,
        lv_rc TYPE i.

  CALL METHOD cl_gui_frontend_services=>file_open_dialog
    EXPORTING
      window_title      = 'Выберите файл'
      default_extension = '*.xls'
      file_filter       = 'Excel-файлы (*.xlsx;*.xls)|*.xlsx;*.xls|Прочие (*.*)|*.*'
    CHANGING
      file_table        = lt_files
      rc                = lv_rc.

  CALL METHOD cl_gui_cfw=>flush.

  IF lines( lt_files ) > 0.
    READ TABLE lt_files INTO p_file INDEX 1.
  ENDIF.
ENDFORM.

FORM start_of_selection.
  PERFORM set_mode.
  PERFORM check_authority.
  IF sy-subrc = 0.
    CASE gv_mode.
      WHEN gc_view_mode.
        PERFORM call_view.
      WHEN gc_load_mode.
        PERFORM call_load.
      WHEN gc_corr_mode.
        PERFORM call_view.
    ENDCASE.
  ENDIF.
ENDFORM.

FORM set_mode.
  CLEAR gv_mode.

  CASE 'X'.
    WHEN p_view.
      gv_mode = gc_view_mode.
    WHEN p_load.
      gv_mode = gc_load_mode.
    WHEN p_corr.
      gv_mode = gc_corr_mode.
  ENDCASE.
ENDFORM.

FORM check_authority.
  DATA:
    lt_fictr TYPE tt_fictr,
    lv_fictr LIKE LINE OF lt_fictr,
    lt_zbdds TYPE tt_zbdds,
    lv_zbdds LIKE LINE OF lt_zbdds,
    lv_datbis TYPE fmfctr-datbis,
    lv_actvt TYPE activ_auth.

  REFRESH: gt_fictr, gt_zbdds.
    
  CASE gv_mode.
    WHEN gc_view_mode OR gc_corr_mode.
      lv_actvt = '03'.							" просмотреть
    WHEN gc_load_mode.
      lv_actvt = '60'.							" импортировать
  ENDCASE.

  lv_datbis = |{ sy-datum(4) }1231|.
  SELECT fmfctr~fictr
    FROM fmfctr
    INTO TABLE lt_fictr
    WHERE fmfctr~fikrs = p_bukrs
      AND fmfctr~fictr IN so_fictr
      AND fmfctr~datbis >= lv_datbis.

  SELECT zfmbdds~zbdds
    FROM zfmbdds
    INTO TABLE lt_zbdds
    WHERE zfmbdds~zbdds IN so_zbdds.

  LOOP AT lt_fictr INTO lv_fictr.
    AUTHORITY-CHECK OBJECT 'ZFMPLANBP'
      ID 'BUKRS' FIELD p_bukrs
      ID 'FISTL' FIELD lv_fictr
      ID 'ACTVT' FIELD lv_actvt.
    IF sy-subrc = 0.
      APPEND lv_fictr TO gt_fictr.
    ENDIF.
  ENDLOOP.

  LOOP AT lt_zbdds INTO lv_zbdds.
    AUTHORITY-CHECK OBJECT 'ZFMPLANBP'
      ID 'BUKRS' FIELD p_bukrs
      ID 'ZBDDS' FIELD lv_zbdds
      ID 'ACTVT' FIELD lv_actvt.
    IF sy-subrc = 0.
      APPEND lv_zbdds TO gt_zbdds.
    ENDIF.
  ENDLOOP.
ENDFORM.


FORM load_xls USING po_log TYPE REF TO lcl_appl_log.
  DATA:
    ls_data TYPE ts_planbp_xls,
    lv_fictr LIKE LINE OF gt_fictr,
    ls_planbp LIKE LINE OF gt_planbp,
    lo_progress TYPE REF TO lcl_progress,
    lo_xls  TYPE REF TO lcl_xls_reader,
    lx_message TYPE REF TO lcx_message,
    lx_root TYPE REF TO cx_root,
    lv_errmsg TYPE string,
    lv_eof  TYPE abap_bool,
    ls_messtab TYPE lcl_appl_log=>t_message.

  PERFORM verify_file_exists USING p_file.
  IF sy-subrc <> 0.
    ls_messtab-msgid = 'ZFIAA'.
    ls_messtab-msgty = 'E'.
    ls_messtab-msgno = '009'.
    ls_messtab-msgv1 = p_file.
    po_log->add_message( ls_messtab ).
    RETURN.
  ENDIF.

  lo_progress = lcl_progress=>create( lcl_progress=>c_progress_random ).

  lo_xls = lcl_xls_reader=>create( ).
  lo_xls->open( EXPORTING p_fname = p_file p_startrow = 2 ).
  DO.
    TRY.
        lo_xls->read_to( CHANGING ps_data = ls_data eof = lv_eof ).
      CATCH lcx_message INTO lx_message.
        ls_messtab = lx_message->bal_s_msg( ).
        po_log->add_message( ls_messtab ).
      CATCH cx_root INTO lx_root.
        ls_messtab-msgid = 'ZFIAA'.
        ls_messtab-msgty = 'E'.
        ls_messtab-msgno = '000'.
        ls_messtab-msgv1 = lx_root->get_longtext( ).
        po_log->add_message( ls_messtab ).
    ENDTRY.

    IF lv_eof = abap_true.
      EXIT.
    ENDIF.

    READ TABLE gt_fictr INTO lv_fictr WITH KEY table_line = ls_data-fictr.
    IF sy-subrc = 0.
      MOVE-CORRESPONDING ls_data TO ls_planbp.
      TRY.
          PERFORM fill_planbp_ext CHANGING ls_planbp.
          PERFORM append_to_planbp USING ls_planbp.
        CATCH lcx_message INTO lx_message.
          CLEAR ls_messtab.
          ls_messtab-msgid = 'ZFIAA'.
          ls_messtab-msgty = 'E'.
          ls_messtab-msgno = '000'.
          ls_messtab-msgv1 = |В строке { lo_xls->rownum( ) }: |.
          ls_messtab-msgv2 = lx_message->get_longtext( ).
          po_log->add_message( ls_messtab ).
      ENDTRY.
    ELSE.
      CLEAR ls_messtab.
      ls_messtab-msgid = 'ZFIAA'.
      ls_messtab-msgty = 'E'.
      ls_messtab-msgno = '011'.
      ls_messtab-msgv1 = |{ lo_xls->rownum( ) }|.
      ls_messtab-msgv2 = ls_data-fictr.
      po_log->add_message( ls_messtab ).
    ENDIF.

    lo_progress->next_indicate( '' ).
  ENDDO.
  lo_progress->stop_indicate( ).
  lo_xls->close( ).

  CHECK po_log->has_messages( ) <> abap_true.
  PERFORM save_planbp.
ENDFORM.


FORM fill_fcat_for_itab USING pt_table TYPE ANY TABLE pt_fields TYPE lvc_t_fcat.
  DATA:
    lo_type   TYPE REF TO cl_abap_typedescr,
    lo_table  TYPE REF TO cl_abap_tabledescr,
    lo_struct TYPE REF TO cl_abap_structdescr,
    lv_name   TYPE string.

  lo_type ?= cl_abap_typedescr=>describe_by_data( pt_table ).
  IF lo_type->kind = cl_abap_typedescr=>kind_table.
    lo_table ?= lo_type.
    lo_struct ?= lo_table->get_table_line_type( ).
  ELSE.
    MESSAGE 'Неверный тип параметра fill_fcat_for_itab' TYPE 'X'.
  ENDIF.
  PERFORM fill_struct USING lo_struct CHANGING pt_fields.
ENDFORM.

FORM fill_struct USING po_struct TYPE REF TO cl_abap_structdescr CHANGING pt_fields TYPE lvc_t_fcat.
  DATA:
    lo_type   TYPE REF TO cl_abap_typedescr,
    lo_struct TYPE REF TO cl_abap_structdescr,
    lo_elem   TYPE REF TO cl_abap_elemdescr,
    lt_fields TYPE cl_abap_structdescr=>component_table,
    ls_field  TYPE cl_abap_structdescr=>component.

  lt_fields = po_struct->get_components( ).
  LOOP AT lt_fields INTO ls_field.
    lo_type ?= ls_field-type.

    IF ls_field-as_include = 'X'.
      IF lo_type IS BOUND.
        CASE lo_type->kind.
          WHEN cl_abap_typedescr=>kind_struct.    " Структура
            lo_struct ?= lo_type.
            PERFORM fill_struct USING lo_struct CHANGING pt_fields.
          WHEN cl_abap_typedescr=>kind_table.     " Таблица
            " ???
        ENDCASE.
      ENDIF.
    ELSE.
      IF lo_type->kind = cl_abap_typedescr=>kind_elem.  " Элементы
        lo_elem ?= lo_type.
        PERFORM fill_element USING ls_field-name lo_elem CHANGING pt_fields.
      ENDIF.
    ENDIF.
  ENDLOOP.
ENDFORM.

ORM fill_element
    USING pv_name TYPE string po_elem TYPE REF TO cl_abap_elemdescr
    CHANGING pt_fields TYPE lvc_t_fcat.

  DATA: ls_field LIKE LINE OF pt_fields,
        ls_dfies TYPE dfies.

  CHECK pv_name <> 'MANDT'.

  CLEAR ls_field.
  IF po_elem->is_ddic_type( ) = abap_true.
    ls_dfies = po_elem->get_ddic_field( sy-langu ).
    MOVE-CORRESPONDING ls_dfies TO ls_field.
  ELSE.
    ls_field-intlen = po_elem->length.
    ls_field-inttype = po_elem->type_kind.
    ls_field-outputlen = po_elem->length.
  ENDIF.
  ls_field-fieldname = pv_name.
  APPEND ls_field TO pt_fields.
ENDFORM.
