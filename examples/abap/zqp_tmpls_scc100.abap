" Экран с ALV-гридом
"
CLASS lcl_scc100_grid DEFINITION DEFERRED.

CLASS lcl_scc100 DEFINITION.
  PUBLIC SECTION.
    CLASS-DATA:
      ref TYPE REF TO lcl_scc100,
      ok_code TYPE sy-ucomm.

    CLASS-METHODS:
      call_screen.

    METHODS:
      constructor,
      leave_screen,
      update_screen,
      user_command,
      set_status.

  PRIVATE SECTION.
    DATA:
      tmpls TYPE REF TO lcl_templates,
      grid TYPE REF TO lcl_scc100_grid.

    METHODS:
      create_tmpl,
      edit_tmpl,
      view_tmpl,
      delete_tmpl,
      copy_tmpl,
      activate_tmpl,
      deactivate_tmpl.
ENDCLASS.

CLASS lcl_scc100_grid DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor IMPORTING ctrl TYPE c tmpls TYPE REF TO lcl_templates,
      update,
      invalidate,
      get_selected_tmpln RETURNING VALUE(res) TYPE zqp_templates-tmpln,
      set_selected_tmpln IMPORTING tmpln TYPE zqp_templates-tmpln.

  PRIVATE SECTION.
    DATA:
      tmpls TYPE REF TO lcl_templates,
      init_mode TYPE abap_bool,
      ctrl TYPE c LENGTH 20,
      grid TYPE REF TO cl_gui_alv_grid,
      container TYPE REF TO cl_gui_custom_container,
      fieldcat TYPE lvc_t_fcat,
      invalidated TYPE abap_bool,
      sel_tmpln TYPE zqp_templates-tmpln.

    METHODS:
      init,
      set_field_catalog.
ENDCLASS.


*** IMPLEMENTATION ***************************************************

CLASS lcl_scc100 IMPLEMENTATION.

METHOD call_screen.
  CREATE OBJECT ref.
  CALL SCREEN 0100.
ENDMETHOD.

METHOD constructor.
  CREATE OBJECT tmpls.
  CREATE OBJECT grid EXPORTING ctrl = 'GRID' tmpls = tmpls.
ENDMETHOD.

METHOD leave_screen.
  CASE sy-ucomm.
    WHEN 'EXIT' OR 'STOP'.
      LEAVE PROGRAM.
    WHEN 'BACK'.
      LEAVE TO SCREEN 0.
  ENDCASE.
ENDMETHOD.

METHOD update_screen.
  grid->update( ).
ENDMETHOD.

METHOD user_command.
  DATA: l_code TYPE sy-ucomm.

  l_code = ok_code.
  CLEAR ok_code.

  CASE l_code.
    WHEN 'NEW_TMPL'.
      create_tmpl( ).
    WHEN 'EDIT_TMPL'.
      edit_tmpl( ).
    WHEN 'VIEW_TMPL'.
      view_tmpl( ).
    WHEN 'DEL_TMPL'.
      delete_tmpl( ).
    WHEN 'COPY_TMPL'.
      copy_tmpl( ).
    WHEN 'SET_ACT'.
      activate_tmpl( ).
    WHEN 'UNSET_ACT'.
      deactivate_tmpl( ).
  ENDCASE.
ENDMETHOD.

METHOD set_status.
  IF ss_edit IS NOT INITIAL.
    SET PF-STATUS 'SE100'.
  ELSE.
    SET PF-STATUS 'SV100'.
  ENDIF.
  SET TITLEBAR 'T100'.
ENDMETHOD.

METHOD create_tmpl.
  DATA: l_tmpln TYPE zqp_templates-tmpln.

  l_tmpln = lcl_scc200=>call_screen( ).
  grid->set_selected_tmpln( l_tmpln ).
  grid->invalidate( ).
ENDMETHOD.

METHOD edit_tmpl.
  DATA: l_tmpln TYPE zqp_templates-tmpln.

  l_tmpln = grid->get_selected_tmpln( ).
  IF l_tmpln IS NOT INITIAL.
    lcl_scc200=>call_screen( l_tmpln ).
    grid->set_selected_tmpln( l_tmpln ).
    grid->invalidate( ).
  ENDIF.
ENDMETHOD.

METHOD view_tmpl.
  DATA: l_edit LIKE ss_edit,
        l_tmpln TYPE zqp_templates-tmpln.

  l_edit = ss_edit.
  CLEAR ss_edit.
  l_tmpln = grid->get_selected_tmpln( ).
  IF l_tmpln IS NOT INITIAL.
    lcl_scc200=>call_screen( l_tmpln ).
    grid->set_selected_tmpln( l_tmpln ).
  ENDIF.
  ss_edit = l_edit.
ENDMETHOD.

METHOD delete_tmpl.
  DATA: l_tmpln TYPE zqp_templates-tmpln,
        l_answer TYPE c.

  CALL FUNCTION 'POPUP_TO_CONFIRM'
  EXPORTING
    titlebar              = ''
    text_question         = 'Вы действительно хотите удалить шаблон?'
    text_button_1         = 'Да'
    text_button_2         = 'Нет'
    display_cancel_button = ''
  IMPORTING
    answer                = l_answer.

  IF sy-subrc = 0 AND l_answer = '1'.
    l_tmpln = grid->get_selected_tmpln( ).
    IF l_tmpln IS NOT INITIAL.
      tmpls->delete( l_tmpln ).
      grid->invalidate( ).
    ENDIF.
  ENDIF.
ENDMETHOD.

METHOD copy_tmpl.
ENDMETHOD.

METHOD activate_tmpl.
  DATA: l_tmpln TYPE zqp_templates-tmpln.

  l_tmpln = grid->get_selected_tmpln( ).
  IF l_tmpln IS NOT INITIAL.
    tmpls->activate( l_tmpln ).
    grid->set_selected_tmpln( l_tmpln ).
    grid->invalidate( ).
  ENDIF.
ENDMETHOD.

METHOD deactivate_tmpl.
  DATA: l_tmpln TYPE zqp_templates-tmpln.

  l_tmpln = grid->get_selected_tmpln( ).
  IF l_tmpln IS NOT INITIAL.
    tmpls->deactivate( l_tmpln ).
    grid->set_selected_tmpln( l_tmpln ).
    grid->invalidate( ).
  ENDIF.
ENDMETHOD.

ENDCLASS.


CLASS lcl_scc100_grid IMPLEMENTATION.

METHOD constructor.
  init_mode = abap_true.
  me->ctrl = ctrl.
  me->tmpls = tmpls.
  invalidated = abap_true.
ENDMETHOD.

METHOD init.
  CREATE OBJECT container EXPORTING container_name = ctrl.
  CREATE OBJECT grid EXPORTING i_parent = container.
  set_field_catalog( ).
  grid->set_table_for_first_display( CHANGING it_outtab = tmpls->rows it_fieldcatalog = fieldcat ).
  init_mode = abap_false.
ENDMETHOD.

METHOD update.
  IF invalidated = abap_true.
    tmpls->select( ).
    invalidated = abap_false.
  ENDIF.

  IF init_mode = abap_true.
    init( ).
  ELSE.
    grid->refresh_table_display( ).
    IF sel_tmpln IS NOT INITIAL.
      DATA: ls_row LIKE LINE OF tmpls->rows.
      READ TABLE tmpls->rows INTO ls_row WITH KEY tmpln = sel_tmpln.

      IF sy-subrc = 0.
        DATA: _rows TYPE LVC_T_ROW,
              _row LIKE LINE OF _rows.

        _row-index = sy-tabix.
        APPEND _row TO _rows.
        grid->set_selected_rows( EXPORTING it_index_rows = _rows ).
      ENDIF.
    ENDIF.
  ENDIF.
  CLEAR sel_tmpln.
ENDMETHOD.

DEFINE _SCC100_GRID_ADD_COL.
  &2-fieldname = &3.
  &2-outputlen = &4.
  &2-scrtext_s = &5.
  &2-scrtext_m = &6.
  &2-scrtext_l = &7.
  &2-no_zero   = 'X'.
  APPEND &2 to &1.
END-OF-DEFINITION.

METHOD set_field_catalog.
  DATA: ls_fcat type lvc_s_fcat.

  _SCC100_GRID_ADD_COL fieldcat ls_fcat 'TMPLN'    8   'ИдШаблона'   'Ид.Шаблона'   'Ид.Шаблона'.
  _SCC100_GRID_ADD_COL fieldcat ls_fcat 'TMPLNAME' 20  'ИмяШаблона'  'Имя Шаблона'  'Имя Шаблона'.
  _SCC100_GRID_ADD_COL fieldcat ls_fcat 'MATNR'    10  'Материал'    'Материал'     'Материал'.
  _SCC100_GRID_ADD_COL fieldcat ls_fcat 'MAKTX'    40  'Наимен'      'Наименование' 'Наименование'.
  _SCC100_GRID_ADD_COL fieldcat ls_fcat 'WERKS'    10  'Завод'       'Завод'        'Завод'.
  _SCC100_GRID_ADD_COL fieldcat ls_fcat 'NAME1'    20  'Имя'         'Имя'          'Имя'.
  _SCC100_GRID_ADD_COL fieldcat ls_fcat 'ACTIVE'   10  'Активный'    'Активный'     'Активный'.
  _SCC100_GRID_ADD_COL fieldcat ls_fcat 'OBJID'    10  'Шаблон'      'Шаблон'       'Шаблон'.
  _SCC100_GRID_ADD_COL fieldcat ls_fcat 'ZCOMMENT' 10  'Комментарий' 'Комментарий'  'Комментарий'.
ENDMETHOD.

METHOD invalidate.
  invalidated = abap_true.
ENDMETHOD.

METHOD get_selected_tmpln.
  DATA: l_tmpln LIKE zqp_templates-tmpln,
        row TYPE i,
        ls_tmpl LIKE LINE OF tmpls->rows.

  res = 0.
  grid->get_current_cell( IMPORTING e_row = row ).
  IF row IS NOT INITIAL.
    READ TABLE tmpls->rows INTO ls_tmpl INDEX row.
    IF sy-subrc = 0.
      res = ls_tmpl-tmpln.
    ENDIF.
  ENDIF.
ENDMETHOD.

METHOD set_selected_tmpln.
  sel_tmpln = tmpln.
ENDMETHOD.

ENDCLASS.