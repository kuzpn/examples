" Чтение XLS файла в произвольную структуру
"
CLASS lcl_xls_reader DEFINITION.
  PUBLIC SECTION.
    TYPES:
      t_filename TYPE c LENGTH 1024.

    CLASS-METHODS:
      create RETURNING VALUE(ref) TYPE REF TO lcl_xls_reader.

    METHODS:
      open IMPORTING p_fname TYPE t_filename p_startrow TYPE i p_cntrows TYPE i OPTIONAL,
      read_to CHANGING ps_data TYPE ANY eof TYPE abap_bool RAISING lcx_message,
      rownum RETURNING VALUE(res) TYPE i,
      reset_rownum,
      close.

  PRIVATE SECTION.
    TYPES:
      BEGIN OF ts_datadescr,
        ref   TYPE REF TO data,
        descr TYPE REF TO lcl_structdescr,
      END OF ts_datadescr.

    DATA: lo_app      TYPE ole2_object,
          lo_wbooks   TYPE ole2_object,
          lo_wbook    TYPE ole2_object,
          lo_cell     TYPE ole2_object,
          mv_startrow TYPE i,
          mv_maxcnt   TYPE i,
          mv_count    TYPE i,
          row         TYPE i,
          value       TYPE string,

          mt_datadescr TYPE HASHED TABLE OF ts_datadescr WITH UNIQUE KEY ref.
ENDCLASS.


DEFINE _TEST_SUBRC.
  IF sy-subrc <> 0.
    MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
         WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
  ENDIF.
END-OF-DEFINITION.


CLASS lcl_xls_reader IMPLEMENTATION.

METHOD open.
  " IMPORTING p_fname TYPE t_filename p_startrow TYPE i p_cntrows TYPE i
  DATA: ref TYPE REF TO data.
  FIELD-SYMBOLS: <fs_maxcnt> LIKE mv_maxcnt.

  CREATE OBJECT lo_app 'Excel.Application'.
  _TEST_SUBRC.
  SET PROPERTY OF lo_app 'DisplayAlerts' = 0.
  _TEST_SUBRC.
  GET PROPERTY OF lo_app 'Workbooks' = lo_wbooks.
  _TEST_SUBRC.
  CALL METHOD OF lo_wbooks 'Open' = lo_wbook EXPORTING #1 = p_fname.
  _TEST_SUBRC.

  mv_startrow = p_startrow - 1.
  IF p_cntrows IS SUPPLIED.
    mv_maxcnt = p_cntrows.
  ELSE.
    ref = cl_abap_exceptional_values=>get_max_value( mv_maxcnt ).
    ASSIGN ref->* TO <fs_maxcnt>.
    mv_maxcnt = <fs_maxcnt>.
  ENDIF.
  reset_rownum( ).
ENDMETHOD.

METHOD create.
  " RETURNING VALUE(ref) TYPE REF TO lcl_xls_reader
  CREATE OBJECT ref.
ENDMETHOD.

METHOD read_to.
  " CHANGING ps_data TYPE ANY eof TYPE abap_bool

  DATA:
    lv_ref TYPE REF TO data,
    ls_datadescr LIKE LINE OF mt_datadescr,
    lo_structdescr TYPE REF TO lcl_structdescr,
    ls_field TYPE lcl_structdescr=>ts_field.

  SUBTRACT 1 FROM mv_count.
  IF mv_count = 0.
    eof = abap_true.
    EXIT.
  ENDIF.
  ADD 1 TO row.

  " Проверяем есть ли созданный объект lcl_structdescr для ps_data по ссылке lv_ref, если нет - добавляем в mt_datadescr
  GET REFERENCE OF ps_data INTO lv_ref.
  READ TABLE mt_datadescr INTO ls_datadescr WITH KEY ref = lv_ref.
  IF sy-subrc = 0.
    lo_structdescr ?= ls_datadescr-descr.
  ELSE.
    CREATE OBJECT lo_structdescr EXPORTING p_instruct = ps_data.
    ls_datadescr-ref = lv_ref.
    ls_datadescr-descr = lo_structdescr.
    INSERT ls_datadescr INTO TABLE mt_datadescr .
  ENDIF.

  DATA:
    lx_error TYPE REF TO cx_root,
    lv_errmsg TYPE symsgv,
    lx_message TYPE REF TO lcx_message,
    col TYPE i VALUE 0,
    value TYPE string.

  FIELD-SYMBOLS:
    <fs_value> TYPE ANY.

  CLEAR: ps_data, eof.
  LOOP AT lo_structdescr->mt_fields INTO ls_field.
    ADD 1 TO col.

    CALL METHOD OF lo_app 'Cells' = lo_cell EXPORTING #1 = row #2 = col.
    CALL METHOD OF lo_cell 'Select'.
    GET PROPERTY OF lo_cell 'Text' = value.

    ASSIGN COMPONENT ls_field-name OF STRUCTURE ps_data TO <fs_value>.
    IF <fs_value> IS ASSIGNED.
      TRY.
        lcl_converter=>str_to_val(
            EXPORTING p_type = ls_field-type
                      p_str = value
            CHANGING p_val = <fs_value> ).
      CATCH cx_root INTO lx_error.
        DATA: lv_row TYPE symsgv,
              lv_col TYPE symsgv.

        WRITE row TO lv_row LEFT-JUSTIFIED.
        WRITE col TO lv_col LEFT-JUSTIFIED.
        lv_errmsg = lx_error->get_longtext( ).
        lx_message = lcx_message=>create( msgid = 'ZFIAA' msgno = '008' msgv1 = lv_row msgv2 = lv_col msgv3 = lv_errmsg ).
        RAISE EXCEPTION lx_message.
      ENDTRY.
      UNASSIGN <fs_value>.
    ENDIF.
  ENDLOOP.

  IF ps_data IS INITIAL.
    eof = abap_true.
  ENDIF.
ENDMETHOD.

METHOD rownum.
  " RETURNING VALUE(res) TYPE i
  res = row.
ENDMETHOD.

METHOD reset_rownum.
  row = mv_startrow.
  mv_count = mv_maxcnt.
ENDMETHOD.

METHOD close.
  IF lo_wbook IS NOT INITIAL.
    CALL METHOD OF lo_wbook 'Close'.
    FREE OBJECT lo_wbook.
  ENDIF.

  IF lo_wbooks IS NOT INITIAL.
    FREE OBJECT lo_wbooks.
  ENDIF.

  IF lo_app IS NOT INITIAL.
    CALL METHOD OF lo_app 'Quit'.
    FREE OBJECT lo_app.
  ENDIF.
ENDMETHOD.


ENDCLASS.
