" Вывод отчета в Excel
"

DATA: merged_rows TYPE TABLE OF i.

CLASS zcl_handle_report DEFINITION.
  PUBLIC SECTION.
    METHODS:
      on_before_fill FOR EVENT before_fill OF zcl_wwwform IMPORTING sender,
      after_row_fill FOR EVENT after_row_fill OF zcl_wwwform
        IMPORTING it_vals range sender.

  PRIVATE SECTION.
    DATA: app TYPE ole2_object,
          top_row TYPE i,
          idx TYPE i,
          rows TYPE i.
    TYPES:
      c2 TYPE c LENGTH 2.

    METHODS:
      merge_cell IMPORTING top TYPE i bottom TYPE i col TYPE i,
      get_colname IMPORTING p_col TYPE i RETURNING VALUE(p_colname) TYPE c2.
ENDCLASS.

CLASS zcl_handle_report IMPLEMENTATION.
  METHOD on_before_fill.
    me->app = sender->get_app( ).
  ENDMETHOD.

  METHOD after_row_fill.
    DATA: cur_row TYPE i.

    IF rows IS INITIAL.
      ADD 1 TO idx.
      READ TABLE merged_rows INTO rows INDEX idx.
      CHECK sy-subrc = 0.
      GET PROPERTY OF range 'Row' = top_row.
    ENDIF.

    SUBTRACT 1 FROM rows.
    IF rows IS INITIAL.
      GET PROPERTY OF range 'Row' = cur_row.
      IF cur_row - top_row > 0.
        DO 9 TIMES.
          merge_cell( EXPORTING top = top_row bottom = cur_row col = sy-index ).
        ENDDO.
      ENDIF.
    ENDIF.
  ENDMETHOD.

  METHOD merge_cell.
    DATA: app_range TYPE ole2_object,
          range TYPE ole2_object,
          colname TYPE string,
          arg TYPE string.

    colname = me->get_colname( col ).
    arg = |{ colname }{ top }:{ colname }{ bottom }|.
    CALL METHOD OF app 'Range' = range EXPORTING #1 = arg.
    CHECK range IS NOT INITIAL.
    CALL METHOD OF range 'Merge' EXPORTING #1 = 0.
  ENDMETHOD.

  METHOD get_colname.
    DATA: d TYPE i,
          r TYPE i,
          cnt TYPE i,
          cnt2 TYPE i.

    CLEAR p_colname.
    cnt = strlen( sy-abcde ).
    cnt2 = 2 * cnt.
    CHECK p_col IS NOT INITIAL AND p_col <= cnt2.   " max = 'ZZ'

    d = p_col DIV cnt.
    r = p_col MOD cnt.
    IF d IS NOT INITIAL.
      p_colname = sy-abcde+d(1).
    ENDIF.
    IF r IS NOT INITIAL.
      r = r - 1.
      CONCATENATE p_colname sy-abcde+r(1) INTO p_colname.
    ENDIF.
  ENDMETHOD.
ENDCLASS.



FORM display_excel.
  DATA: lt_vals TYPE zwww_values_t.

  CHECK lines( gt_repdata ) > 0.
  PERFORM fill_www_vals CHANGING lt_vals.

  DATA: lo_rep TYPE REF TO zcl_wwwform,
        lo_event TYPE REF TO zcl_handle_report.

  CREATE OBJECT lo_rep.
  CREATE OBJECT lo_event.
  SET HANDLER lo_event->on_before_fill FOR lo_rep.
  SET HANDLER lo_event->after_row_fill FOR lo_rep.

  lo_rep->openform(
    EXPORTING
      form_name     = 'ZTRM_REP_0001'
      printdialog   = ''
      protect       = ''
      print         = ''
      no_out        = ''
      debug_mode    = ''
    CHANGING
      it_values     = lt_vals ).

  IF sy-subrc <> 0.
    MESSAGE ID SY-MSGID TYPE SY-MSGTY NUMBER SY-MSGNO
      WITH SY-MSGV1 SY-MSGV2 SY-MSGV3 SY-MSGV4.
  ENDIF.
ENDFORM.


DEFINE _add_header_val.
  CLEAR &3.
  &3-find_text = &1.
  &3-value     = &2.
  APPEND &3 TO &4.
END-Of-DEFINITION.

DEFINE _add_row_val.
  CLEAR &5.
  &5-var_name  = &4.
  &5-var_num   = &3.
  &5-find_text = &1.
  IF &2 IS NOT INITIAL.
    &5-value     = &2.
  ELSE.
    &5-value     = space.
  ENDIF.
  APPEND &5 TO &6.
END-OF-DEFINITION.

DEFINE _date_to_str.
  IF &1 IS NOT INITIAL.
    WRITE &1 TO &2.
  ELSE.
    CLEAR &2.
  ENDIF.
END-OF-DEFINITION.

FORM fill_www_vals CHANGING pt_vals TYPE zwww_values_t.
  DATA: ls_val LIKE LINE OF pt_vals,
        ls_repdata LIKE LINE OF gt_repdata,
        l_rfha LIKE ls_repdata-rfha,
        l_str TYPE c LENGTH 10,
        l_rows TYPE i,
        l_znum TYPE i,
        idx TYPE i.

  DATA: sum_bbasis1 LIKE ls_repdata-bbasis,
        sum_bbasis2 LIKE ls_repdata-bzbetr,
        sum_bbasis3 LIKE ls_repdata-bbasis,
        sum_bnwhr   LIKE ls_repdata-bnwhr.

  READ TABLE s_dzterm INDEX 1.
  IF sy-subrc = 0.
    WRITE s_dzterm-low TO l_str.
    _add_header_val '(DATEBEG)' l_str ls_val pt_vals.
    WRITE s_dzterm-high TO l_str.
    _add_header_val '(DATEEND)' l_str ls_val pt_vals.
  ENDIF.

  CLEAR: l_rfha, l_rows.
  SORT gt_repdata BY rfha bbasis DESCENDING.

  LOOP AT gt_repdata INTO ls_repdata.
    idx = sy-tabix.

    IF l_rfha NE ls_repdata-rfha.
      IF l_rows IS NOT INITIAL.
        APPEND l_rows TO merged_rows.
      ENDIF.
      ADD 1 TO l_znum.
      l_rfha = ls_repdata-rfha.
      l_rows = 1.

      _add_row_val '(ZNUM)'   l_znum            idx 'ROWS' ls_val pt_vals.
    ELSE.
      ADD 1 TO l_rows.

      _add_row_val '(ZNUM)'   space             idx 'ROWS' ls_val pt_vals.
    ENDIF.

    _add_row_val '(KONTRH)'   ls_repdata-kontrh idx 'ROWS' ls_val pt_vals.
    _add_row_val '(FIO)'      ls_repdata-fio    idx 'ROWS' ls_val pt_vals.
    _add_row_val '(RFHA)'     ls_repdata-rfha   idx 'ROWS' ls_val pt_vals.
    _add_row_val '(ZUOND)'    ls_repdata-zuond  idx 'ROWS' ls_val pt_vals.

"	................................
"	................................

    _add_row_val '(PKOND)'    ls_repdata-pkond  idx 'ROWS' ls_val pt_vals.
    _add_row_val '(BNWHR)'    ls_repdata-bnwhr  idx 'ROWS' ls_val pt_vals.

    IF ls_repdata-dbervon = s_dzterm-low.
      ADD ls_repdata-bbasis TO sum_bbasis1.
    ENDIF.
    IF ls_repdata-sberfima = 'TZ' OR ls_repdata-sberfima = 'TTEN' AND ls_repdata-dbervon = s_dzterm-low.
      ADD ls_repdata-bbasis TO sum_bbasis3.
    ENDIF.
    IF ls_repdata-dzterm2 = s_dzterm-high.
      SUBTRACT ls_repdata-bzbetr FROM sum_bbasis3.
    ENDIF.

    ADD ls_repdata-bzbetr TO sum_bbasis2.
    ADD ls_repdata-bnwhr TO sum_bnwhr.
  ENDLOOP.
  IF l_rows IS NOT INITIAL.
    APPEND l_rows TO merged_rows.
  ENDIF.

  _add_header_val '(SUM_BBASIS1)' sum_bbasis1 ls_val pt_vals.
  _add_header_val '(SUM_BBASIS2)' sum_bbasis2 ls_val pt_vals.
  _add_header_val '(SUM_BBASIS3)' sum_bbasis3 ls_val pt_vals.
  _add_header_val '(SUM_BNWHR)'   sum_bnwhr  ls_val pt_vals.
ENDFORM.
