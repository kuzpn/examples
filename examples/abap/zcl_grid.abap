" Вывод таблицы на основе cl_salv_table 
"
CLASS zcl_grid DEFINITION.
  PUBLIC SECTION.
    CLASS-METHODS:
      display.

  PRIVATE SECTION.
    CLASS-DATA:
      lo_alv_table TYPE REF TO cl_salv_table.

    CLASS-METHODS:
      set_layout,
      set_columns,
      add_hotspot
        IMPORTING
          po_cols TYPE REF TO cl_salv_columns_table
          col_name TYPE c,

      set_column
        IMPORTING
          po_col    TYPE REF TO cl_salv_column
          p_lname   TYPE c OPTIONAL
          p_mname   TYPE c OPTIONAL
          p_sname   TYPE c OPTIONAL
          p_len     TYPE lvc_outlen OPTIONAL
          p_align   TYPE salv_de_alignment OPTIONAL
          p_visible TYPE c OPTIONAL
          p_hotspot TYPE c OPTIONAL.
ENDCLASS.

CLASS zcl_grid IMPLEMENTATION.

METHOD display.
  DATA: lo_events TYPE REF TO cl_salv_events_table,
        lo_handler TYPE REF TO lcl_handle_function.

  cl_salv_table=>factory(
    IMPORTING
        r_salv_table = lo_alv_table
    CHANGING t_table = zcl_out=>data ).

  lo_alv_table->set_screen_status( pfstatus = 'SALV_STATUS'
  report = sy-repid  set_functions = lo_alv_table->c_functions_all ).

  set_layout( ).
  set_columns( ).

  lo_events = lo_alv_table->get_event( ).
  CREATE OBJECT lo_handler.
  SET HANDLER lo_handler->on_added_salv_function FOR lo_events.
  SET HANDLER lo_handler->on_link_click FOR lo_events.


  lo_alv_table->refresh( ).
  lo_alv_table->display( ).
ENDMETHOD.

METHOD set_layout.
  DATA: lo_layout TYPE REF TO cl_salv_layout,
        keys TYPE salv_s_layout_key.

  lo_layout = lo_alv_table->get_layout( ).
  keys-report = sy-repid.
  lo_layout->set_key( keys ).
  lo_layout->set_save_restriction( '3' ).
ENDMETHOD.

METHOD set_columns.
  DATA: fldname TYPE lvc_fname,
        idx TYPE n LENGTH 2,
        lo_cols TYPE REF TO cl_salv_columns_table,
        lt_cols TYPE salv_t_column_ref,
        ls_col TYPE salv_s_column_ref,
        lo_col TYPE REF TO cl_salv_column.

  lo_cols = lo_alv_table->get_columns( ).
  IF lo_cols IS NOT BOUND. RETURN. ENDIF.

  lt_cols = lo_cols->get( ).
  LOOP AT lt_cols INTO ls_col.
    lo_col = ls_col-r_column.
    IF lo_col IS NOT BOUND. CONTINUE. ENDIF.

    CASE lo_col->get_columnname( ).
      WHEN 'BELNR'.
        set_column( EXPORTING po_col = lo_col
          p_lname = 'Номер док. выравнивания' p_mname = '№ выравнивания' p_sname = 'НомДок' p_len = 12 ).
        add_hotspot( EXPORTING po_cols = lo_cols col_name = 'BELNR' ).

      WHEN 'GJAHR'.
        set_column( EXPORTING po_col = lo_col
          p_lname = 'Фин.год документа выравнивания' p_mname = 'Год выравнивания' p_sname = 'ГодВырав' p_len = 8 ).

      WHEN 'BUDAT'.
        set_column( EXPORTING po_col = lo_col
          p_lname = 'Дата проводки документа выравнивания' p_mname = 'Дата выравнивания' p_sname = 'ДатВырав' ).

*		WHEN '...'
*      	.....................
    ENDCASE.
  ENDLOOP.
ENDMETHOD.

METHOD add_hotspot.
  DATA: lo_col TYPE REF TO cl_salv_column_table.

  lo_col ?= po_cols->get_column( col_name ).
  IF lo_col IS BOUND.
    lo_col->set_cell_type( if_salv_c_cell_type=>hotspot ).
  ENDIF.
ENDMETHOD.

METHOD set_column.
  IF p_lname IS SUPPLIED.   po_col->set_long_text( p_lname ).     ENDIF.
  IF p_mname IS SUPPLIED.   po_col->set_medium_text( p_mname ).   ENDIF.
  IF p_sname IS SUPPLIED.   po_col->set_short_text( p_sname ).    ENDIF.
  IF p_len   IS SUPPLIED.   po_col->set_output_length( p_len ).   ENDIF.
  IF p_align IS SUPPLIED.   po_col->set_alignment( p_align ).     ENDIF.
  IF p_visible IS SUPPLIED. po_col->set_visible( p_visible ).     ENDIF.
ENDMETHOD.

ENDCLASS.
