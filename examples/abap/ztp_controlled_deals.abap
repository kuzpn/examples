"=======================================================================
" FORMS ...
"=======================================================================
FORM check_parameters.
  IF so_dtbeg > so_dtend.   " Если 'Дата с' больше чем 'Дата по', то меняем их местами
    DATA: dt LIKE so_dtbeg.
    dt = so_dtbeg.
    so_dtbeg = so_dtend.
    so_dtend = dt.
  ENDIF.
ENDFORM.

FORM select_contracts.
  CHECK so_doknr IS NOT INITIAL AND so_dokar IS NOT INITIAL.

  TYPES: BEGIN OF ts_contract_key,
           id TYPE ztp_contr_deals-_id,
           doknr TYPE draw-doknr,
           dokar TYPE draw-dokar,
           dokvr TYPE draw-dokvr,
           doktl TYPE draw-doktl,
         END OF ts_contract_key,

         BEGIN OF ts_select_row.
           INCLUDE TYPE ts_contract_key AS rrp RENAMING WITH SUFFIX _rrp.
           INCLUDE TYPE ts_contract_key AS drp RENAMING WITH SUFFIX _drp.
           TYPES:
             vbeln TYPE vbrk-vbeln,
             fkart TYPE vbrk-fkart,
             inco1 TYPE vbrk-inco1,
             posnr TYPE vbrp-posnr,
             lmeng TYPE vbrp-lmeng,
             netwr TYPE vbrp-netwr,
             dokcnt TYPE ztp_contr_deals-dokcnt,
         END OF ts_select_row.

  DATA: lr_fkdat TYPE RANGE OF vbrk-fkdat WITH HEADER LINE,
        ls_contr_deals LIKE LINE OF gt_contr_deals,
        ls_select_row TYPE ts_select_row,
        ls_cur_rrp TYPE ts_contract_key,
        l_rrp_id TYPE ztp_contr_deals-_id.

  lr_fkdat-sign = 'I'.
  lr_fkdat-option = 'BT'.
  lr_fkdat-low = so_dtbeg.
  lr_fkdat-high = so_dtend.
  APPEND lr_fkdat.

  enumerator=>reset( ).
  SELECT
      rrp~doknr AS doknr_rrp      " Договор
      rrp~dokar AS dokar_rrp
      rrp~dokvr AS dokvr_rrp
      rrp~doktl AS doktl_rrp

      drp~doknr AS doknr_drp      " Доп.соглашение
      drp~dokar AS dokar_drp
      drp~dokvr AS dokvr_drp
      drp~doktl AS doktl_drp

      MAX( vbrk~inco1 ) AS inco1  " Счет-фактура
      SUM( vbrp~fklmg ) AS lmeng
      SUM( vbrp~netwr ) AS netwr
      COUNT( * )        AS dokcnt

    FROM vbrk
      INNER JOIN vbrp ON vbrp~vbeln = vbrk~vbeln
      INNER JOIN draw AS drp ON drp~doknr = vbrk~zuonr
      INNER JOIN draw AS rrp ON rrp~doknr = drp~prenr
        AND rrp~dokar = drp~prear
        AND rrp~dokvr = drp~prevr
        AND rrp~doktl = drp~pretl
    INTO CORRESPONDING FIELDS OF ls_select_row
    WHERE
      ( ( drp~doknr IN so_doknr AND drp~dokar IN so_dokar AND
          drp~dokvr = so_dokvr  AND drp~doktl = so_doktl )
        OR
        ( rrp~doknr IN so_doknr AND rrp~dokar IN so_dokar AND
          rrp~dokvr = so_dokvr  AND rrp~doktl = so_doktl )
      )
      AND vbrp~matnr = so_matnr
      AND vbrk~fkdat IN lr_fkdat
      AND vbrk~fktyp = 'L'
      AND vbrk~vbtyp = 'M'
      AND ( vbrk~rfbsk <> 'E' AND vbrk~fksto <> 'X' )
      AND vbrp~lmeng > 0
    GROUP BY rrp~doknr rrp~dokar rrp~dokvr rrp~doktl
             drp~doknr drp~dokar drp~dokvr drp~doktl
    ORDER BY rrp~doknr drp~doknr.

    IF ls_cur_rrp-doknr <> ls_select_row-doknr_rrp.
      " Добавим в gt_contr_deals договор
      CLEAR ls_contr_deals.
      l_rrp_id                  = enumerator=>next( ).
      ls_contr_deals-_id        = l_rrp_id.
      ls_contr_deals-_parent_id = 0.
      ls_contr_deals-_isfolder  = 'X'.
      ls_contr_deals-doknr      = ls_select_row-doknr_rrp.
      ls_contr_deals-dokar      = ls_select_row-dokar_rrp.
      ls_contr_deals-dokvr      = ls_select_row-dokvr_rrp.
      ls_contr_deals-doktl      = ls_select_row-doktl_rrp.

      APPEND ls_contr_deals TO gt_contr_deals.
      ls_cur_rrp = ls_select_row-rrp.
    ENDIF.

    " Добавим в gt_contr_deals доп.соглашение
    CLEAR ls_contr_deals.
    ls_contr_deals-_id        = enumerator=>next( ).
    ls_contr_deals-_parent_id = l_rrp_id.               " Ссылка на договор
    ls_contr_deals-_isfolder  = ''.
    ls_contr_deals-doknr      = ls_select_row-doknr_drp.
    ls_contr_deals-dokar      = ls_select_row-dokar_drp.
    ls_contr_deals-dokvr      = ls_select_row-dokvr_drp.
    ls_contr_deals-doktl      = ls_select_row-doktl_drp.
    ls_contr_deals-dokcnt     = ls_select_row-dokcnt.
    ls_contr_deals-lfimg      = ls_select_row-lmeng.
    IF ls_select_row-lmeng IS NOT INITIAL.
      ls_contr_deals-netwr    = ls_select_row-netwr / ls_select_row-lmeng.
    ENDIF.
    ls_contr_deals-incoterms  = ls_select_row-inco1.
    APPEND ls_contr_deals TO gt_contr_deals.
  ENDSELECT.

  DATA: tmp_cnt TYPE i.
  CLEAR ls_contr_deals.
  PERFORM fill_more USING ls_contr_deals CHANGING tmp_cnt.
  PERFORM read_matname.
ENDFORM.


FORM fill_more USING ps_contr_deals LIKE LINE OF gt_contr_deals CHANGING p_cnt TYPE i.
  FIELD-SYMBOLS: <fs_row> LIKE LINE OF gt_contr_deals.

  LOOP AT gt_contr_deals ASSIGNING <fs_row> WHERE _parent_id = ps_contr_deals-_id.
    PERFORM fill_node USING <fs_row>.
    PERFORM fill_more USING <fs_row> CHANGING <fs_row>-dokcnt.
    ps_contr_deals-lfimg = ps_contr_deals-lfimg + <fs_row>-lfimg.

    " Если не заполнено, берём из вышестоящего документа
    IF <fs_row>-payterm IS INITIAL.
      <fs_row>-payterm = ps_contr_deals-payterm.
    ENDIF.
    " или из нижестоящего документа
    IF ps_contr_deals-incoterms IS INITIAL.
      ps_contr_deals-incoterms = <fs_row>-incoterms.
      ps_contr_deals-incoterms_txt = <fs_row>-incoterms_txt.
    ENDIF.
    ADD 1 TO p_cnt.
  ENDLOOP.
ENDFORM.

FORM read_matname.
  CHECK so_matnr IS NOT INITIAL.

  CLEAR g_maktx.
  SELECT SINGLE makt~maktx
    FROM makt
    INTO g_maktx
    WHERE makt~matnr = so_matnr
    	AND makt~spras = sy-langu.
ENDFORM.

FORM fill_node USING ps_node LIKE LINE OF gt_contr_deals.
  DATA: res TYPE i.

  res = contract_info=>read(
      p_doknr = ps_node-doknr  p_dokar = ps_node-dokar
      p_dokvr = ps_node-dokvr  p_doktl = ps_node-doktl
    ).
  IF res = 0.
    ps_node-data        = contract_info=>get_data( ).         " Дата
    ps_node-valuta      = contract_info=>get_valuta( ).       " Валюта
    ps_node-payterm     = contract_info=>get_payterm( ).      " Условия платежа
    ps_node-date_from   = contract_info=>get_date_from( ).    " Дата с
    ps_node-date_to     = contract_info=>get_date_to( ).      " Дата по
    ps_node-_region     = contract_info=>get_region( ).       " Регион, населенный пункт
  ENDIF.
  ps_node-_month        = monther=>get_name( ps_node-date_from ).        " Название месяца
  ps_node-payterm_txt   = payterms_text=>get_text( ps_node-payterm ).    " Тексты
  ps_node-incoterms_txt = incoterms_text=>get_text( ps_node-incoterms ).

  IF ps_node-dokar = 'DRP'.           " Если это доп.соглашение читаем инф-ю из специф-ии
    PERFORM fill_spec USING ps_node.
  ENDIF.
ENDFORM.

FORM fill_spec USING ps_item LIKE LINE OF gt_contr_deals.
  DATA: l_vbeln TYPE vbak-vbeln.

  SELECT SINGLE
      vbak~vbeln
      vbak~waerk
    FROM vbak
    INTO (l_vbeln, ps_item-waerk)
    WHERE vbak~zuonr = ps_item-doknr
      AND vbak~vbtyp = 'G'.

  IF l_vbeln IS NOT INITIAL.
    SELECT
        SUM( vbap~zmeng ) AS zmeng
        MAX( vbap~zieme ) AS zieme    " А как иначе?
      FROM vbap
      INTO (ps_item-zmeng, ps_item-zieme)
      WHERE vbap~vbeln = l_vbeln
        AND vbap~spart = '01'.
  ENDIF.
ENDFORM.

FORM cut_contract_num CHANGING p_doknr TYPE draw-doknr.
  DATA: len TYPE i.

  len = strlen( p_doknr ).
  IF len > 3.
    len = len - 3.
    p_doknr = p_doknr+len(3).
  ENDIF.
ENDFORM.
