create table last_export (
    id integer primary key,
    type char(8) not null,
    owner varchar(80) not null,
    name varchar(80) not null,
    crdate timestamp not null
)

create unique index idx_obj_uniq on last_export (owner, name)


create table table_row_count (
    name varchar(80) not null,
    date timestamp not null,
    general_rows int not null,
    created_rows int,
    modified_rows int
)
