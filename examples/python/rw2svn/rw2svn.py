__author__="Kuzmin_PN"
__date__ ="$30.12.2008 8:25:25$"

import pysvn
import pyodbc
import os
import os.path
import datetime
import settings
import sqlite3

#-------------------------------------------------------------------------------

class SvnClient:
    def __init__(self, repos):
        self.repos = repos
        if (self.repos[-1:] <> '/') and (self.repos[-1:] <> '\\'):
            self.repos = self.repos + '/'

        if not os.path.exists(self.repos):
            raise Exception("Can't find path: %s" % self.repos)
        
        self.client = pysvn.Client()

    def __get_paths(self, files):
        paths = []
        if files != None:
            paths = [(self.repos + f) for f in files]
        else:
            paths = self.repos
        return paths

    def add(self, files):
        self.client.add(self.__get_paths(files))

    def remove(self, files):
        self.client.remove(self.__get_paths(files), True)

    def update(self, files=None):
        return self.client.update(self.__get_paths(files))

    def commit(self, message, files=None):
        return self.client.checkin(self.__get_paths(files), message)
    
    def list(self):
        return self.client.ls(self.repos)

    def status(self):
        return self.client.status(self.repos, recurse=True, get_all=True)

#-------------------------------------------------------------------------------

class DbObject:
    def __init__(self):
        self.type = 'O'
        self.owner = ''
        self.name = ''
        self.crdate = ''
        self.filename = ''
        self.filepath = ''

class DbTable(DbObject):
    def __init__(self):
        self.type = 'U'
        self.owner = 'dbo'
        self.name = ''
        self.crdate = ''
        self.filename = ''
        self.filepath = 'tables/'

class DbTrig(DbObject):
    def __init__(self):
        self.type = 'TR'
        self.owner = 'dbo'
        self.name = ''
        self.crdate = ''
        self.filename = ''
        self.filepath = 'trigs/'

class DbProc(DbObject):
    def __init__(self):
        self.type = 'P'
        self.owner = 'dbo'
        self.name = ''
        self.crdate = ''
        self.filename = ''
        self.filepath = 'procs/'
        self.version = 1

#-------------------------------------------------------------------------------

class DbClient:
    def __init__(self, conn_str):
        self.conn = pyodbc.connect(conn_str)
        self.cursor = self.conn.cursor()

    def list_procs(self):
    	sql = """
    		select o.id, u.name as owner, o.name, o.crdate, c.number
    		from sysobjects o
       			inner join syscomments c on c.id = o.id
                inner join sysusers u on u.uid = o.uid
            where o.type = 'P'
            group by o.id, u.name, o.name, o.crdate, c.number
            order by o.id, c.number """

        def alloc_dbproc(row):
            proc = DbProc()
            proc.owner = row.owner
            proc.name = row.name
            proc.crdate = row.crdate
            proc.version = row.number
#            proc.filename = row.name + ';' + str(row.number) + '.sql'
            proc.filename = row.name + '_' + str(row.number) + '.sql'
            proc.filepath += proc.filename
            return proc

        return [alloc_dbproc(o) for o in self.cursor.execute(sql).fetchall()]

    def list_tables(self):
        sql = """
            select o.id, u.name as owner, o.name, o.crdate
            from sysobjects o
                inner join sysusers u on u.uid = o.uid
            where o.type = 'U'
            order by o.name """
            
        def alloc_dbtable(row):
            table = DbTable()
            table.owner = row.owner
            table.name = row.name
            table.crdate = row.crdate
            table.filename = row.name + '.sql'
            table.filepath += table.filename
            return table

        return [alloc_dbtable(o) for o in self.cursor.execute(sql).fetchall()]

    def list_trigs(self):
        sql = """
            select o.id, u.name as owner, o.name, o.crdate
            from sysobjects o
                inner join sysusers u on u.uid = o.uid
            where o.type = 'TR'
            order by o.name
        """
    
        def alloc_dbtrig(row):
            trig = DbTrig()
            trig.owner = row.owner
            trig.name = row.name
            trig.crdate = row.crdate
            trig.filename = row.name + '.sql'
            trig.filepath += trig.filename
            return trig

        return [alloc_dbtrig(o) for o in self.cursor.execute(sql).fetchall()]

    def list_objects(self):
        objs = []
        objs.extend(self.list_tables())
        objs.extend(self.list_trigs())
        objs.extend(self.list_procs())
        
        return objs

    def proc_text(self, procname, version):
    	sql = """
    		select o.id, c.text
    		from sysobjects o
    			inner join syscomments c on c.id = o.id
    		where o.id = object_id(?) and c.number = ?
    		order by c.colid """
    	text = ""
    	for row in self.cursor.execute(sql, procname, version):
    		text += row.text

        return text

    # fkey = 0 - if primary key
    # fkey = 1 - if foreign key
    def _table_key_fields(self, row, fkey, indention):
        text = ""
        for idx in xrange(row.cColCount):
            if idx > 0:
                text += ",\n"
            text += indention + row[7 + fkey*16 + idx]
        return text

    def _table_pkey_type(self, row):
        text = ""
        if row.cType == 1:
            if row.cFlags == 131073:
                text = "clustered"
            elif row.cFlags == 131072:
                text = "nonclustered"
        return text

    def _table_keys(self, tablename, indention):
        sql = " sp_MStablekeys ?, null, 14 "

        text = ""
        for row in self.cursor.execute(sql, tablename):
            text += ",\n"
            if row.cType == 1:
                text += "%sconstraint %s primary key %s (\n%s\n%s)" % \
                    (indention, row.cName, self._table_pkey_type(row), \
                    self._table_key_fields(row, 0, '\t\t'), indention)
            elif row.cType == 3:
                text += "%sconstraint %s foreign key (\n%s\n%s) references %s (\n%s\n%s)" % \
                    (indention, row.cName, self._table_key_fields(row, 0, '\t\t'), indention, row.cRefTable, \
                    self._table_key_fields(row, 1, '\t\t'), indention)
        return text

    def _table_idx_fields(self, row):
        text = ""
        i = 0
        while row[4 + i] != None:
            if i > 0:
                text += ","
            text += row[4 + i]
            i = i + 1
            
        return text

    def _table_indexes(self, tablename):
        sql = " sp_MShelpindex ? "
        text = ""

        for row in self.cursor.execute(sql, tablename):
#            if row.status != 10770 and row.status != 2178 and row.status != 2050:
            if row.status < 1000:
                if row.status == 2 or row.status == 130:
                    text += "create unique"
                else:
                    text += "create"
                text += " index %s on %s(%s)\ngo\n" % (row.name, tablename, self._table_idx_fields(row))

        return text

    def _table_fields(self, tablename, indention):
        sql = " exec sp_MShelpcolumns ?, @orderby = 'id' "
        text = ""
    	for row in self.cursor.execute(sql, tablename):
            if row.col_id > 1:
                text += ",\n"

            # type
            text += "%s%s %s" % (indention, row.col_name, row.DataType)
            if row.BaseType in ['char', 'varchar'] :
                text += "(%i)" % row.col_len
            elif row.BaseType in ['decimal', 'numeric']:
                text += "(%i,%i)" % (row.col_prec, row.col_scale)

            # identity
            if row.col_identity == 1:
                text += " identity (%i,%i)" % (row.col_seed, row.col_increment)

            # null
            if row.col_null == 0:
                text += " not"
            text += " null"

            # constraint
            if row.col_dridefname != None:
                text += " constraint %s %s" % (row.col_dridefname, row.col_drideftext)

        return text

    def _table_trig_names(self, tablename):
        sql = """
            select u.name as owner, o.name, td.name as deltrig, ti.name as instrig, tu.name as updtrig
            from sysobjects o
                inner join sysusers u on u.uid = o.uid
                left join sysobjects td on td.id = o.deltrig and td.type = 'TR'
                left join sysobjects ti on ti.id = o.instrig and ti.type = 'TR'
                left join sysobjects tu on tu.id = o.updtrig and tu.type = 'TR'
            where o.type = 'U' and o.id = object_id(?) """
        row = self.cursor.execute(sql, tablename).fetchone()
        text = ""
        if row.deltrig != None:
            text += "\n-- trigger on delete: " + row.deltrig
        if row.instrig != None:
            text += "\n-- trigger on insert: " + row.instrig
        if row.updtrig != None:
            text += "\n-- trigger on update: " + row.updtrig

        return text

    def table_text(self, tablename):
    	text = "create table %s (\n%s%s\n)\ngo\n" % \
            (tablename, self._table_fields(tablename, '\t'), self._table_keys(tablename, '\t'))
        text += self._table_indexes(tablename)
        text += self._table_trig_names(tablename) + '\n'
        
        return text

    def trig_text(self, trigname):
        sql = """
            if exists (
                select *
                from syscomments
                where id=object_id('dbo.tI_rwtank_group')
                    and texttype & 4 <> 0
            )
                select text='<Encrypted>', number=0, type=''
                else
                    select c.text, c.number, o.type
                    from syscomments c, sysobjects o
                    where o.id = c.id
                          and c.id = object_id(?)
                    order by c.colid
        """
        text = ""
        for row in self.cursor.execute(sql, trigname).fetchall():
            text += row.text

        return text

    def obj_text(self, obj):
        text = ""
        
        if obj.type == 'U':
            text = self.table_text(obj.name)
        elif obj.type == 'TR':
            text = self.trig_text(obj.name)
        elif obj.type == 'P':
            text = self.proc_text(obj.name, obj.version)
            
        return text.replace('\r', '')

#-------------------------------------------------------------------------------

class WcFiles:
    def __init__(self, svnclient):
        def is_file_versioned(f):
            return f.is_versioned and f.entry.name != '.'

        self.path = svnclient.repos
        self.files = [f.path[len(self.path):] for f in svnclient.status() if is_file_versioned(f)]

    def find(self, filename):
        return filename in self.files

    def remove(self, filename):
        if self.find(filename):
            self.files.remove(filename)

    def list(self):
        return self.files

    def writefile(self, filename, text):
        filepath = os.path.join(self.path, filename)
        f = open(filepath, 'w')
        try:
            f.write(text)
        finally:
            f.close()

#-------------------------------------------------------------------------------

class Last:
    def __init__(self, dbpath):
        self.dbpath = "./"
        self.conn = sqlite3.connect(dbpath)
        self.cur = self.conn.cursor()

    def getLastDate(self, type, owner, name, create_new=True):
        sql = """
            select id, type, owner, name, crdate
            from last_export
            where type = ?
              and owner = ?
              and name = ?
        """
        row = self.cur.execute(sql, (type, owner, name)).fetchone()
        if (row == None) and (create_new):
            self.setLastDate(type, owner, name, datetime.datetime(1980, 1, 1))
            res = self.getLastDate(type, owner, name, create_new=False)
        else:
            res = row[4]

        return res

    def setLastDate(self, type, owner, name, lastdate):
        sql = """
            select count(*) as cnt
            from last_export
            where type = ?
              and owner = ?
              and name = ?
        """
        row = self.cur.execute(sql, (type, owner, name)).fetchone()
        if row[0] > 0:
            sql = """
                update last_export
                set crdate = ?
                where type = ?
                  and owner = ?
                  and name = ?
            """
        else:
            sql = """
                insert into last_export (crdate, type, owner, name)
                values (?, ?, ?, ?)
            """
        self.cur.execute(sql, (lastdate.replace(microsecond=0), type, owner, name))
        self.conn.commit()

#-------------------------------------------------------------------------------

def main():
    #from_date = settings.from_date
    wc_path = settings.wc_path
    lastdb_path = settings.lastdb_path

    last_crdate = Last(lastdb_path)

    svn = SvnClient(wc_path)
    svn.update()
    svn.commit('Start: ' + str(datetime.datetime.now()))

    wc = WcFiles(svn)
    db = DbClient(settings.conn_string)
    objs = db.list_objects()
    for obj in objs:
        filename = os.path.normpath(obj.filepath)
        filepath = os.path.normpath(wc_path + filename)
        dt_str = last_crdate.getLastDate(obj.type, obj.owner, obj.name)
        dt = datetime.datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S')

        if not wc.find(filename):
            print 'Add: ', filename
            wc.writefile(filepath, db.obj_text(obj))
            svn.add([filename])
        elif obj.crdate.replace(microsecond=0) > dt:
            print 'Update: ', filename
            wc.writefile(filepath, db.obj_text(obj))
            last_crdate.setLastDate(obj.type, obj.owner, obj.name, obj.crdate)
        wc.remove(filename)

    for f in wc.list():
        print 'Delete: ', f
    svn.remove(wc.list())
    
    svn.commit('End: ' + str(datetime.datetime.now()))


if __name__ == "__main__":
    main()
    print "Ok.";
