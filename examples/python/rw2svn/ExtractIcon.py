# -*- coding: utf-8 -*-
__author__="kuzmin_pn"
__date__ ="$30.07.2009 10:45:00$"

import sys
from PyQt4 import QtGui, QtCore
import resicon

class MyIcon(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        vbox = QtGui.QVBoxLayout()

        self.open = QtGui.QPushButton(self.tr("Open"))
        self.connect(self.open, QtCore.SIGNAL("clicked()"), self.onOpen)
        self.save = QtGui.QPushButton(self.tr("Save"))
        self.connect(self.save, QtCore.SIGNAL("clicked()"), self.onSave)
        self.exit = QtGui.QPushButton(self.tr("Exit"))
        self.connect(self.exit, QtCore.SIGNAL("clicked()"), self.onExit)

        self.iconSize = 32  # ������ ������
        self.list = QtGui.QListWidget(self)
        self.list.setIconSize(QtCore.QSize(self.iconSize, self.iconSize))
        hbox = QtGui.QHBoxLayout()
        hbox.addWidget(self.open)
        hbox.addWidget(self.save)
        hbox.addWidget(self.exit)

        vbox.addWidget(self.list)
        vbox.addLayout(hbox)
        self.setLayout(vbox)


    def onOpen(self):
        filter_exe = "Executable Modules (*.exe *.dll *.cpl)"
        filter_all = "All Files (*)"

        filename = QtGui.QFileDialog.getOpenFileName(self, self.tr("Open module"), "./",
            self.tr(filter_all + ";;" + filter_exe), self.tr(filter_exe));

        if(filename != None):
            self.loadModule(str(filename))

    def onSave(self):
        item = self.list.currentItem()
        if(item == None):
            return

        filter_jpg = "Jpeg Files (*.jpg)"
        filter_gif = "Gif Files (*.gif)"
        filter_bmp = "Bmp Files (*.bmp)"
        filter_png = "Bmp Files (*.png)"
        filter_all = "All Files (*)"
        filename = QtGui.QFileDialog.getSaveFileName(self, self.tr("Save Icon"), "./",
            self.tr(filter_all + ";;" +
            filter_jpg + ";;" +
            filter_gif + ";;" +
            filter_bmp + ";;" +
            filter_png), self.tr(filter_jpg))
            
        if(filename !=  None):
            icon = item.icon()
            pix = icon.pixmap(self.iconSize)
            pix.save(filename, None)


    def onExit(self):
        self.close()

    def loadModule(self, filename):
        self.list.clear()
        for i in xrange(resicon.geticoncnt(filename)):
            icon = QtGui.QIcon()
            icon.addPixmap(QtGui.QPixmap.fromWinHBITMAP(resicon.geticon(filename, i)))
            item = QtGui.QListWidgetItem(icon, self.tr("index") + str(i))
            self.list.addItem(item)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    mainwnd = MyIcon()
    mainwnd.show()
    sys.exit(app.exec_())