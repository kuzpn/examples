#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <stdexcept>
#include <elephant.h>


namespace mytask {

const char* TestWordsFile       = "build/file1.txt";
const char* TestDictFile        = "build/file2.txt";
const char* TestNotExistingFile = "xxxxxx.txt";
const char* TestEmptySourceFile = "build/test_empty_source.txt";
const char* TestEmptyDestFile   = "build/test_empty_dest.txt";

const StringSet TestDict        = {"КОТ","ТОН","РОТ","ТОТ"};
const StringList TestCorrectRes = {"КОТ", "ТОТ", "ТОН"};


struct Initializer
{
    Initializer() {
        std::locale current_locale("");
        std::locale::global(current_locale);
        std::ios::sync_with_stdio(false);
    }
};

BOOST_GLOBAL_FIXTURE (Initializer);


BOOST_AUTO_TEST_SUITE (TestElephantParam)

    BOOST_AUTO_TEST_CASE (testNotExistingWordsFile)
    {
            BOOST_CHECK_THROW(readFromFiles(TestNotExistingFile, TestDictFile), std::runtime_error);
    }

    BOOST_AUTO_TEST_CASE (testNotExistingDictFile)
    {
            BOOST_CHECK_THROW(readFromFiles(TestWordsFile, TestNotExistingFile), std::runtime_error);
    }

    BOOST_AUTO_TEST_CASE (testEmptyWordsFile)
    {
            BOOST_CHECK_THROW(readFromFiles(TestEmptySourceFile, TestDictFile), std::logic_error);
            BOOST_CHECK_THROW(readFromFiles(TestEmptyDestFile, TestDictFile), std::logic_error);
    }

    BOOST_AUTO_TEST_CASE (testCorrectFiles)
    {
        Elephant::Tuple param = readFromFiles(TestWordsFile, TestDictFile);
        BOOST_CHECK_EQUAL(std::get<0>(param), "КОТ");
        BOOST_CHECK_EQUAL(std::get<1>(param), "ТОН");

        StringSet &inp = std::get<2>(param);
        BOOST_CHECK_EQUAL_COLLECTIONS(inp.begin(), inp.end(), TestDict.begin(), TestDict.end());
    }

BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE (TestElephant)

    BOOST_AUTO_TEST_CASE (testUnreachableDest)
    {
        Elephant elephant("КОТ", "ЫЫЫ", TestDict);
        StringList res = elephant.make();
        BOOST_CHECK_EQUAL(res.size(), 0);
    }

    BOOST_AUTO_TEST_CASE (testGoodElephant)
    {
        Elephant elephant("КОТ", "ТОН", TestDict);
        StringList res = elephant.make();
        BOOST_CHECK_EQUAL_COLLECTIONS(res.begin(), res.end(), TestCorrectRes.begin(), TestCorrectRes.end());
    }

BOOST_AUTO_TEST_SUITE_END()

}   // namespace mytask
