#ifndef ELEPHANT_H
#define ELEPHANT_H

#include <fstream>
#include <string>
#include <tuple>
#include <list>
#include <set>
#include <map>

namespace mytask {

typedef std::set<std::string> StringSet;
typedef std::list<std::string> StringList;
typedef size_t StrHash;

// Ф-ия преобразования строки из wide в multibyte
std::wstring WStrFromStr(const std::string& str);
// Ф-ия преобразовнаия строки из multibyte в wide
std::string StrFromWStr(const std::wstring& str);


class Elephant
{
public:
    // тип входного параметра конструктора
    typedef std::tuple<std::string, std::string, StringSet> Tuple;

    Elephant(const std::string& src, const std::string& dst, const StringSet& dict);
    Elephant(const Elephant::Tuple& param);
    const StringList& make();

    const std::string source()     { return StrFromWStr(src); }
    const std::string destination() { return StrFromWStr(dst); }

private:
    std::wstring src;   // начальное слово
    std::wstring dst;   // конечное слово
    StringList result;  // результат. список слов
    std::hash<std::wstring> fnHash; // ф-ия вычисляющая хэш строки
    StrHash hSrc;       // хэш-код исходного слова
    StrHash hDst;       // хэш конечного слова

    std::map<StrHash, std::wstring> dict;       // словарь слов
    std::multimap<StrHash, StrHash> patterns;   // шаблоны словарных слов

    void verifySrcDst();
    void verifyDict();
    void createLocalDict(const StringSet& dict);    
    int calcDistance(const std::wstring& word); // считает количество несовпавших символов
                                                // с конечным словом
};


Elephant::Tuple readFromFiles(const std::string& file1, const std::string& file2);

}	// namespace mytask


#endif // ELEPHANT_H
