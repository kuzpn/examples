#include <iostream>
#include <cstdlib>
#include <elephant.h>

#ifdef __linux__
#include <libgen.h>
#endif

using namespace mytask;


void displayUsage(char *appname)
{
    std::cout << "Пример: ";
#ifdef _WIN32
    char filename[_MAX_FNAME];
    _splitpath(appname, NULL, NULL, filename, NULL);
    std::cout << filename;
#elif __linux__
    std::cout << basename(appname);
#endif
    std::cout << " file1 file2\n\n"
        << "  file1 - файл с начальным и конечным словами\n"
        << "  file2 - файл словаря"
        << std::endl;
}



int main (int argc, char* argv[])
{
    std::locale current_locale("");
    std::locale::global(current_locale);
    std::ios::sync_with_stdio(false);

    if (argc != 3) {
        displayUsage(argv[0]);
        return EXIT_FAILURE;
    }

    try {
        // чтение исходных параметров и словаря из файлов
        Elephant::Tuple param = readFromFiles(argv[1], argv[2]);
        // объявление объекта-производителя слона
        Elephant elephant(param);

        std::cout << elephant.source() << " -> " << elephant.destination() << ":\n";

        // запускаем процесс производства слона (поиска цепочки слов)
        const StringList &result = elephant.make();
        // выводим результат
        for (auto &word: result) {
            std::cout << "  " << word << "\n";
        }

    } catch(std::exception &err) {
        std::cout << err.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

