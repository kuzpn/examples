#include <iostream>
#include <stdexcept>
#include <queue>
#include <list>
#include <elephant.h>

namespace mytask {

constexpr size_t ConvBufSize = 255;     // размер буфера, используемого при преобразовании строк string <=> wstring

Elephant::Elephant(const std::string& src, const std::string& dst, const StringSet& dict)
{
    // присваиваем значения и вычисляем хэши слов
    this->src = WStrFromStr(src);
    hSrc = fnHash(this->src);
    this->dst = WStrFromStr(dst);
    hDst = fnHash(this->dst);
    // проверяем на соответствие требованиям
    verifySrcDst();

    // на основе переданного словаря заполняем свой внутренний
    createLocalDict(dict);
    // затем проверяем его
    verifyDict();
}

Elephant::Elephant(const Elephant::Tuple& param)
{
    src = WStrFromStr(std::get<0>(param));
    hSrc = fnHash(src);
    dst = WStrFromStr(std::get<1>(param));
    hDst = fnHash(dst);
    verifySrcDst();

    createLocalDict(std::get<2>(param));
    verifyDict();
}

void Elephant::verifySrcDst()
{
    std::string errmsg;

    if (src.empty() || dst.empty())
        { errmsg = "Не указано начальное или конечное слово!"; }
    if (src == dst)
        { errmsg = "Начальное слово совпадает с конечным!";	}
    if (src.length() != dst.length())
        { errmsg = "Длина начального слова не совпадает с длиной конечного!"; }

    if (!errmsg.empty())
        { throw std::logic_error(errmsg); }
}

void Elephant::verifyDict()
{
    if (dict.empty())
        { throw std::logic_error("Словарь пуст!"); }
}

void Elephant::createLocalDict(const StringSet& dict)
{
        for (auto ss: dict) {                       // перебираем все слова
            std::wstring word = WStrFromStr(ss);    // преобразуем их в wstring
            if (word.length() != src.length())      // исключаем не подходящие по длине
                { continue; }

            StrHash hash = fnHash(word);            // вычисляем хэш
            this->dict[hash] = word;                // сохраняем во внутреннем словаре

            // последовательно заменяем символы слова на '*', вычисляем хэш
            // и заносим в patterns, со ссылкой на первоначальное слово
            // например для КОТ (хэш - 123):
            //  *ОТ     patterns.insert(546, 123)
            //  К*Т     patterns.insert(764, 123)
            //  КО*     patterns.insert(342, 123)
            for (unsigned int i = 0; i < word.length(); ++i) {
                std::wstring t = word;
                t.replace(i, 1, L"*");
                patterns.insert(std::make_pair(fnHash(t), hash));
            }
	}
}

int Elephant::calcDistance(const std::wstring& word)
{
    // считает количество несовпавших символов у word с конечным словом
    int dist = 0;
    for (unsigned int i = 0; i < word.length() && i < dst.length(); ++i) {
        if (word[i] != dst[i]) { ++dist; }
    }
    return dist;
}

const StringList& Elephant::make()
{
    // при составлении слона используем "Алгоритм поиска А*"

    // структура открытого списка
    struct OpenItem {
        StrHash hWord;
        int F;
    };

    // структура закрытого списка
    struct ClosedItem {
        StrHash hWord;
        StrHash hParent;
        bool visited;
        int F;
        int G;
        int H;
    };

    // ф-ия сравнения значений двух элементов открытого списка (для упорядочения)
    auto cmpFn = [](const OpenItem& item1, const OpenItem& item2) { return item1.F > item2.F; };
    // объявление открытого списка
    std::priority_queue<OpenItem, std::vector<OpenItem>, decltype(cmpFn)> openSet(cmpFn);
    // закрытый список
    std::map<StrHash, ClosedItem> closedSet;
    // флаг сигнализирующий, что конечное слово было достигнуто
    bool res = false;

    // заносим в списки начальное слово
    openSet.push({hSrc, 0});
    closedSet[hSrc] = {hSrc, 0, false, 0, 0, 0};

    while (!openSet.empty()) {          // пока открытый список не пуст
        OpenItem item = openSet.top();  // извлекаем элемент с минимальным значением F
        openSet.pop();
        ClosedItem current = closedSet[item.hWord];
        current.visited = true;         // в закрытом списке помечаем слово как "посещенное"

        if (current.hWord == hDst) {    // достигли конечного слова?
            res = true;                 // выставляем соответствующий флаг
            break;                      // и выходим из цикла
        }

        std::wstring word = dict[current.hWord];    // из словаря по хэшу считываем слово

        // последовательно буквы слова заменяем на '*' (подробнее см. createLocalDict())
        // и ищем слова с похожим шаблоном в patterns по хэшу
        for (unsigned int i = 0; i < word.length(); ++i) {
            std::wstring t = word;
            t.replace(i, 1, L"*");
            StrHash hash = fnHash(t);
            // по всем словам, соответствующим шаблону
            for (auto it = patterns.find(hash); it != patterns.end() && it->first == hash; ++it) {
                StrHash hw = it->second;
                if (hw == item.hWord) { // пропускаем шаблоны слова word
                    continue;
                }

                auto oit = closedSet.find(hw);
                // если слова в уже находится в закрытом списке и помечено как "посещенное",
                // то пропускаем его
                if (oit != closedSet.end() && oit->second.visited) {
                    continue;
                }

                // готовим новый элемент для закрытого списка
                ClosedItem t = {hw, current.hWord, false, 0, 0, 0};
                t.G = current.G + 1;
                t.H = calcDistance(dict[hw]);
                t.F = t.G + t.H;

                if (oit == closedSet.end()) {       // слово отсутствует в списке - добавляем его
                    closedSet[hw] = t;
                    openSet.push({hw, t.F});
                } else if (t.G < oit->second.G) {   // слово есть в списке и не было посещено, сравним и,
                                                    // в случае необходимости, обновим стоимость и родителя
                    oit->second.hParent = current.hWord;
                    oit->second.G = t.G;
                    oit->second.F = oit->second.G + oit->second.H;
                }
            }
        }

    }   // while (!openSet.empty())

    if (res) {  // если конечное слово было достигнуто, то заполняем список result
        auto it = closedSet.find(hDst);
        while (it != closedSet.end()) {
            ClosedItem &item = it->second;
            std::string word = StrFromWStr(dict[item.hWord]);
            result.push_front(word);
            it = (item.hParent != 0 ?  closedSet.find(item.hParent) : closedSet.end());
        }
    }

    return result;
}


std::wstring WStrFromStr(const std::string& str)
{
    wchar_t wcs[ConvBufSize];
    std::size_t res = mbstowcs(wcs, str.c_str(), ConvBufSize);
    if (res <= 0)
        { return std::wstring(); }

    return std::wstring(wcs);
}


std::string StrFromWStr(const std::wstring& str)
{
    char mbs[ConvBufSize];
    std::size_t res = wcstombs(mbs, str.c_str(), ConvBufSize);
    if (res <= 0)
        { return std::string(); }

    return std::string(mbs);
}


Elephant::Tuple readFromFiles(const std::string& file1, const std::string& file2)
{
    std::ifstream fs1(file1);
    if (!fs1) {
        throw std::runtime_error(std::string("Не удалось открыть файл " + file1 +  "!"));
    }

    // чтение слов исходного и конечного
    std::string sword, dword;
    fs1 >> sword;
    if (!fs1.eof()) {
        fs1 >> dword;
    }
    fs1.close();

    if (sword.empty() || dword.empty()) {
        throw std::logic_error("Не указано начальное или конечное слово!");
    }

    std::ifstream fs2(file2);
    if (!fs2) {
        throw std::runtime_error(std::string("Не удалось открыть файл " + file2 +  "!"));
    }

    // чтение словаря
    StringSet dict;
    std::string dictword;
    while (!fs2.eof()) {
        fs2 >> dictword;
        // проверка на пустоту и равенство длины с исходным словом
        if (!dictword.empty() && dictword.length() == sword.length()) {
            dict.insert(dictword);
        }
    }
    fs2.close();

    return std::make_tuple(sword, dword, dict);
}


}	// namespace mytask
