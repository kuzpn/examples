#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    createActions();
    createMenu();
    createToolbar();

    table = new QTableView(this);
    model = new TableModel(this);
    table->setModel(model);
    table->horizontalHeader()->setDefaultSectionSize(250);
    setCentralWidget(table);
    connect(model, &TableModel::changeStatus, this, &MainWindow::modelChanged);

    resize(600, 400);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createActions()
{
    actOpen = new QAction(QIcon(":/images/open.png"), tr("&Open"), this);
    actOpen->setShortcut(QKeySequence::Open);
    actOpen->setStatusTip(tr("Open text file"));
    connect(actOpen, &QAction::triggered, this, &MainWindow::openFile);

    actSaveAs = new QAction(QIcon(":/images/save.png"), tr("&Save As"), this);
    actSaveAs->setShortcut(QKeySequence::SaveAs);
    actSaveAs->setStatusTip(tr("Save text file as"));
    actSaveAs->setEnabled(false);
    connect(actSaveAs, &QAction::triggered, this, &MainWindow::saveFileAs);

    actIns = new QAction(QIcon(":/images/add.png"), tr("Insert row"), this);
    actIns->setShortcut(QKeySequence::New);
    actIns->setStatusTip(tr("Insert new row"));
    connect(actIns, &QAction::triggered, this, &MainWindow::insertRow);

    actDel = new QAction(QIcon(":/images/remove.png"), tr("Remove row"), this);
    actDel->setShortcut(QKeySequence::Delete);
    actDel->setStatusTip(tr("Remove row"));
    connect(actDel, &QAction::triggered, this, &MainWindow::removeRow);
}

void MainWindow::createMenu()
{
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(actOpen);
    fileMenu->addAction(actSaveAs);
    fileMenu->addSeparator();

    QAction *actExit = new QAction(tr("&Exit"), this);
    connect(actExit, &QAction::triggered, this, &QMainWindow::close);
    actExit->setShortcut(QKeySequence::Quit);
    actExit->setStatusTip(tr("Exit the application"));
    fileMenu->addAction(actExit);

    QAction *actAbout = new QAction(tr("&About Qt"), this);
    actAbout->setStatusTip(tr("About Qt dialog"));
    connect(actAbout, &QAction::triggered, qApp, &QApplication::aboutQt);

    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(actAbout);
}

void MainWindow::createToolbar()
{
    ui->toolBar->addAction(actOpen);
    ui->toolBar->addAction(actSaveAs);
    ui->toolBar->addAction(actIns);
    ui->toolBar->addAction(actDel);
}

void MainWindow::openFile()
{
    QString filename;
    filename = QFileDialog::getOpenFileName(this, tr("Select file"), "",
                                            tr("Text files (*.txt);;All files (*.*)"));
    if(!filename.isEmpty()) {
        try {
            model->open(filename);
        } catch (std::exception& err) {
            statusBar()->showMessage(err.what(), 5000);
        }
    }
}

void MainWindow::saveFileAs()
{
    QString filename;
    filename = QFileDialog::getSaveFileName(this, tr("Save file"), "",
                                            tr("Text files (*.txt);;All files (*.*)"));
    if(!filename.isEmpty()) {
        try {
            model->saveAs(filename);
            statusBar()->showMessage(tr("File saved"), 5000);
        } catch (std::exception& err) {
            statusBar()->showMessage(err.what(), 5000);
        }
    }
}

void MainWindow::modelChanged(bool modified)
{
    if (actSaveAs->isEnabled() != modified) {
        actSaveAs->setEnabled(modified);
    }
}

void MainWindow::insertRow()
{
    model->insertRow(table->currentIndex().row(), QModelIndex());
}

void MainWindow::removeRow()
{
    model->removeRow(table->currentIndex().row(), QModelIndex());
}
