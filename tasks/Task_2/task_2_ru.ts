<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Task_2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="32"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>&amp;Open</source>
        <oldsource>Open</oldsource>
        <translation type="unfinished">&amp;Открыть</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="36"/>
        <source>Open text file</source>
        <translation type="unfinished">Открыть текстовый файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="39"/>
        <source>&amp;Save As</source>
        <oldsource>Save As</oldsource>
        <translation type="unfinished">&amp;Сохранить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="41"/>
        <source>Save text file as</source>
        <translation type="unfinished">Сохранить в текстовый файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="45"/>
        <source>Insert row</source>
        <translation type="unfinished">Вставить строку</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="47"/>
        <source>Insert new row</source>
        <translation type="unfinished">Вставить новую строку</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="50"/>
        <location filename="mainwindow.cpp" line="52"/>
        <source>Remove row</source>
        <translation type="unfinished">Удалить строку</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="58"/>
        <source>&amp;File</source>
        <oldsource>File</oldsource>
        <translation type="unfinished">&amp;Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="63"/>
        <source>&amp;Exit</source>
        <oldsource>Exit</oldsource>
        <translation type="unfinished">&amp;Выйти</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="66"/>
        <source>Exit the application</source>
        <translation type="unfinished">Закрыть приложение</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="69"/>
        <source>&amp;About Qt</source>
        <oldsource>About Qt</oldsource>
        <translation type="unfinished">&amp;О Qt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="70"/>
        <source>About Qt dialog</source>
        <translation type="unfinished">Информация о Qt
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="73"/>
        <source>&amp;Help</source>
        <oldsource>Help</oldsource>
        <translation type="unfinished">&amp;Справка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="88"/>
        <source>Select file</source>
        <translation type="unfinished">Выбрать файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="89"/>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Text files (*.txt);;All files (*.*)</source>
        <translation type="unfinished">Текстовые файлы (*.txt);;Все файлы (*.*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="102"/>
        <source>Save file</source>
        <translation type="unfinished">Сохранить файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="107"/>
        <source>File saved</source>
        <translation type="unfinished">Файл сохранён</translation>
    </message>
</context>
<context>
    <name>TableModel</name>
    <message>
        <location filename="tablemodel.cpp" line="41"/>
        <source>Name</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="tablemodel.cpp" line="42"/>
        <source>Phone</source>
        <translation type="unfinished">Телефон</translation>
    </message>
    <message>
        <location filename="tablemodel.cpp" line="43"/>
        <source>???</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="tablemodel.cpp" line="120"/>
        <source>File not found</source>
        <translation type="unfinished">Файл не найден</translation>
    </message>
</context>
</TS>
