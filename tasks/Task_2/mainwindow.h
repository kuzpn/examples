#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableView>
#include "tablemodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QAction *actOpen;
    QAction *actSaveAs;
    QAction *actIns;
    QAction *actDel;
    QTableView *table;
    TableModel *model;

    void createActions();
    void createMenu();
    void createToolbar();
    void openFile();
    void saveFileAs();
    void modelChanged(bool modified);
    void insertRow();
    void removeRow();
};

#endif // MAINWINDOW_H
