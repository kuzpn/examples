#include <fstream>
#include <QfileInfo>
#include "tablemodel.h"

TableModel::TableModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    rows.push_back(std::make_tuple("", ""));
}

int TableModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return rows.size();
}

int TableModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);
    return std::tuple_size<RowType>::value;
}

QVariant TableModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) { return QVariant(); }
    int row = index.row();

    if (role == Qt::DisplayRole && row >= 0 && row < static_cast<int>(rows.size())) {
        switch (index.column()) {
            case 0: return std::get<0>(rows.at(row));
            case 1: return std::get<1>(rows.at(row));
        }
    }
    return QVariant();
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
            case 0: return tr("Name");
            case 1: return tr("Phone");
            default: return tr("???");
        }
    }

    return QVariant();
}

bool TableModel::setData(const QModelIndex & index, const QVariant & value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
        int row = index.row();
        int col = index.column();
        int size = static_cast<int>(rows.size());

        if (row >= 0 && row < size) {
            if (col == 0) {
                std::get<0>(rows.at(row)) = value.toString();
            } else if (col == 1) {
                std::get<1>(rows.at(row)) = value.toString();
            } else {
                return false;
            }

            if (row == size-1) {
                insertRow(rows.size(), QModelIndex());
            }
            emit changeStatus(true);
            return true;
        }
    }
    return false;
}

bool TableModel::insertRow(int row, const QModelIndex& parent)
{
    if (row >= 0) {
        beginInsertRows(parent, row, row);
        if (row < static_cast<int>(rows.size())) {
            rows.insert(rows.begin()+row, std::make_tuple("", ""));
        } else {
            rows.push_back(std::make_tuple("", ""));
        }
        endInsertRows();

        emit changeStatus(true);
        return true;
    }

    return false;
}

bool TableModel::removeRow(int row, const QModelIndex& parent)
{
    if (row >= 0 && row < static_cast<int>(rows.size()-1)) {
        beginRemoveRows(parent, row, row);
        rows.erase(rows.begin()+row);
        endRemoveRows();

        emit changeStatus(true);
        return true;
    }

    return false;
}

Qt::ItemFlags TableModel::flags(const QModelIndex & index) const
{
    if (index.isValid()) {
        return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
    } else {
        return Qt::ItemIsEnabled;
    }
}

void TableModel::open(const QString filename)
{
    if (!QFileInfo::exists(filename)) {
        throw std::logic_error(tr("File not found").toStdString().c_str());
    }

    char buf[250];
    QString qbuf;

    std::ifstream fs(filename.toStdString().c_str());
    beginResetModel();
    rows.clear();
    while (!fs.eof()) {
        fs.getline(buf, sizeof(buf)/sizeof(buf[0]));
        qbuf = QString::fromLocal8Bit(buf);
        if (qbuf.size() == 0) { continue; }

        QStringList slist = qbuf.split(";");
        if (slist.size() > 1) {
            rows.push_back(std::make_tuple(slist.at(0), slist[1]));
        } else {
            rows.push_back(std::make_tuple(slist[0], QString()));
        }
    }
    fs.close();
    rows.shrink_to_fit();
    endResetModel();

    insertRow(rows.size(), QModelIndex());

    emit changeStatus(false);
}


void TableModel::saveAs(const QString filename)
{
    std::ofstream fs(filename.toStdString().c_str());
    QString name;
    QString phone;
    for (auto row: rows) {
        name = std::get<0>(row);
        phone = std::get<1>(row);
        if (name.size() > 0 || phone > 0) {
            fs << name.toLocal8Bit().data()
               << ";"
               << phone.toLocal8Bit().data()
               << '\n';
        }
    }
    fs.close();
    emit changeStatus(false);
}
