#-------------------------------------------------
#
# Project created by QtCreator 2016-10-06T14:42:44
#
#-------------------------------------------------

QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TRANSLATIONS = task_2_ru.ts

TARGET = Task_2
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tablemodel.cpp

HEADERS  += mainwindow.h \
    tablemodel.h

FORMS    += mainwindow.ui

RESOURCES += \
    Task_2.qrc
