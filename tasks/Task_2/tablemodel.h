#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <tuple>
#include <string>
#include <vector>
#include <QAbstractTableModel>

typedef std::tuple<QString, QString> RowType;
typedef std::vector<RowType> RowsType;

class TableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TableModel(QObject *parent = 0);

    int rowCount(const QModelIndex& parent) const;
    int columnCount(const QModelIndex& parent) const;
    QVariant data(const QModelIndex& index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role);
    bool insertRow(int row, const QModelIndex& parent = QModelIndex());
    bool removeRow(int row, const QModelIndex& parent = QModelIndex());
    Qt::ItemFlags flags(const QModelIndex & index) const;

    void open(const QString filename);
    void saveAs(const QString filename);

signals:
    void changeStatus(bool modified);

public slots:

private:
    RowsType rows;
};

#endif // TABLEMODEL_H
