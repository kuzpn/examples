#include <stdio.h>
#include <stdbool.h>
#include "quadratic.h"

bool call_quadratic(qequation *eqs, size_t count)
{
	bool ret;
	char *res;
	if (ret = quadratic(eqs, count)) {
		for (int i = 0; i < count; ++i) {
		
			switch (eqs[i].rescode) {
				case Q_NOROOTS: res = "Q_NOROOTS"; break;
				case Q_ONEROOT: res = "Q_ONEROOT"; break;
				case Q_TWOROOTS: res = "Q_TWOROOTS"; break;
				default: res = "?????";
			}
			
			printf("a = %6.3f, b = %6.3f, c = %6.3f:\tres = %11s   x1 = %f, x2 = %f\n",
				eqs[i].a, eqs[i].b, eqs[i].c, res, eqs[i].x1, eqs[i].x2);
		}
	}
	return ret;
}

#define _call_quadratic(eqs,cnt) 				\
	puts("call_quadratic("#eqs", "#cnt"): {");	\
	puts(call_quadratic(eqs, cnt) ? "}: ok" : "}: fail");


int main()
{
	qequation eqs[] = {
		{.a = 12,	.b = 4,		.c = 5},
		{.a = 0, 	.b = 3,		.c = -9},
		{.a = 2,	.b = 4,		.c = 1},
		{.a = 16,	.b = 9.23,	.c = 3},
		{.a = 12,	.b = 0,		.c = 1},
		{.a = 3.5,	.b = 11,	.c = 0},
		{.a = 3,	.b = 4,		.c = 1},
	};
	size_t count = sizeof(eqs)/sizeof(eqs[0]);

	_call_quadratic(NULL, count);	puts("");
	_call_quadratic(eqs, 0);		puts("");
	_call_quadratic(eqs, count);
	
	return 0;
}
