#include <math.h>
#include "quadratic.h"

bool quadratic(qequation *eqs, size_t count)
{
	if (!eqs) { return false; }
	if (!count) { return true; }
	
	double a, b, c;
	double discr;
	for(int i = 0; i < count; ++i) {
		a = eqs[i].a;
		b = eqs[i].b;
		c = eqs[i].c;
		
		eqs[i].x1 = eqs[i].x2 = NAN;
		if (a != 0) {
			discr = b*b - 4 * a * c;
			if (discr > 0) {	
				double sqrt_discr = sqrt(discr);
				eqs[i].x1 = (-b + sqrt_discr) / (2 * a);
				eqs[i].x2 = (-b - sqrt_discr) / (2 * a);
				eqs[i].rescode = Q_TWOROOTS;
			} else if (discr == 0) {
				eqs[i].x1 = -b / (2 * a);
				eqs[i].rescode = Q_ONEROOT;
			} else {
				eqs[i].rescode = Q_NOROOTS;
			}
		} else {
			if (b != 0) {
				eqs[i].x1 = - c / b;
				eqs[i].rescode = Q_ONEROOT;
			} else {
				eqs[i].rescode = Q_NOROOTS;
			}			
		}
	}
	return true;
}
