#ifndef QUADRATIC_H
#define QUADRATIC_H

#if !(defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901L))
#error required C99
#endif

#include <stdbool.h>

typedef enum qrescode_t {
	Q_NOROOTS,
	Q_ONEROOT,
	Q_TWOROOTS
} qrescode;

typedef struct qequation_t {
	double a, b, c;
	double x1, x2;
	qrescode rescode;
} qequation;


bool quadratic (qequation *eqs, size_t count);

#endif  // QUADRATIC_H