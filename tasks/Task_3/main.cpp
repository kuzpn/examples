#include <stdexcept>
#include <cstring>
#include <functional>
#include <iostream>
#include <windows.h>

#define WM_MOVEBUTTON1      WM_APP+10
#define WM_MOVEBUTTON2      WM_APP+20
#define WM_MOVEBUTTONS12    WM_APP+30

#define ID_BUTTON1          1000
#define ID_BUTTON2          1010

#define TM_BUTTON1          300           // интервалы перемещения
#define TM_BUTTON2          150
#define TM_BUTTONS12        50

typedef struct button_rect_t {
    unsigned int x, y, w, h;
} button_rect;

// положение и размер кнопок
const button_rect button1 = {10, 10, 200, 32};
const button_rect button2 = {button1.x + button1.w + 100, 10, 200, 32};

// ф-ия смещения кнопок на off-пикселов
void moveWindowByOff(HWND hwnd, int off)
{
    WINDOWPLACEMENT pos;
    if (GetWindowPlacement(hwnd, &pos)) {
        SetWindowPos(hwnd, 0, pos.rcNormalPosition.left, pos.rcNormalPosition.top + off,
                     0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_NOACTIVATE);
    }
}


/////////////////// Application //////////////////
class Application {
public:
    Application(HINSTANCE hInstance, std::string name);
    std::string name() const { return appname; }
    int messageLoop();
    static HINSTANCE instance() { return hInst; }
    static DWORD threadId() { return tid; }

    std::function<bool(MSG*)>userTranslation;

private:
    std::string appname;
    static HINSTANCE hInst;
    static DWORD tid;
};

HINSTANCE Application::hInst;
DWORD Application::tid;

Application::Application(HINSTANCE hInstance, std::string name)
{
    Application::hInst = hInstance;
    appname = name;
    tid = GetCurrentThreadId();
}

int Application::messageLoop()
{
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0)) {        
        TranslateMessage(&msg);
        if (userTranslation) {
            userTranslation(&msg);
        }
        DispatchMessage(&msg);
    }
    return msg.wParam;
}


/////////////////// system_error //////////////////
class system_error: public std::runtime_error
{
public:
    system_error(DWORD errCode = GetLastError());
    const char* what() const noexcept { return msgbuf; }
    system_error& operator=(const system_error& other);
private:
    DWORD error;
    char msgbuf[255];
};

system_error::system_error(DWORD errCode): std::runtime_error("")
{
    error = errCode;
    int buflen = sizeof(msgbuf) / sizeof(msgbuf[0]);
    memset(msgbuf, 0, sizeof(msgbuf));

    DWORD res = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, error,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), msgbuf, buflen, NULL);
    if (!res) {
        snprintf(msgbuf, buflen, "Error: %i", (int)error);
    }
}

system_error& system_error::operator=(const system_error& other)
{
    if (this != &other) {
        error = other.error;
        strncpy(msgbuf, other.msgbuf, sizeof(msgbuf) / sizeof(msgbuf[0]));
    }
    return (*this);
}



/////////////////// Ticker //////////////////
class Ticker
{
public:
    Ticker(unsigned int per, UINT msg);
    ~Ticker();
    void stop();

private:
    HANDLE hThread;
    HANDLE hEvent;
    unsigned int tm;
    UINT msg;

    static DWORD WINAPI threadFunc(LPVOID arg);
};

Ticker::Ticker(unsigned int tm, UINT msg): tm(tm), msg(msg)
{
    int err;
    DWORD tid;

    hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (!hEvent) {
        err = GetLastError();
        goto error_evt;
    }

    hThread = CreateThread(NULL, 0, &Ticker::threadFunc, this, 0, &tid);
    if (!hThread) {
        err = GetLastError();
        goto error_thd;
    }
    return;

error_thd:
    CloseHandle(hEvent);
error_evt:
    throw system_error(err);
}

Ticker::~Ticker()
{
    stop();
    Sleep(500);
    if (hThread) CloseHandle(hThread);
    if (hEvent) CloseHandle(hEvent);
}

void Ticker::stop()
{
    SetEvent(hEvent);
}

DWORD WINAPI Ticker::threadFunc(LPVOID arg)
{
    if (arg) {
        Ticker *ticker = reinterpret_cast<Ticker*>(arg);
        while(WaitForSingleObject(ticker->hEvent, ticker->tm) == WAIT_TIMEOUT) {
            PostThreadMessage(Application::threadId(), ticker->msg, 0, 0);
        }
    }
    return 0;
}



/////////////////// MainWindow //////////////////
class MainWindow
{
public:
    MainWindow(std::string name, DWORD style);
    ~MainWindow();

    LRESULT windowProc(UINT uMsg, WPARAM wParam, LPARAM lParam);
    bool userTranslator(MSG* msg);
    void show(int cmdShow = SW_SHOWDEFAULT);

    std::function<void(HWND)> onButton1Click;
    std::function<void(HWND)> onButton2Click;
    std::function<void(HWND)> onButton1Move;
    std::function<void(HWND)> onButton2Move;
    std::function<void(HWND, HWND)> onButtons12Move;
private:
    WNDCLASSEX wcls;
    std::string wcls_name;
    HWND hwnd;
    HWND hButton1;
    HWND hButton2;
    bool constructed;

    static LRESULT CALLBACK wndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
};

MainWindow::~MainWindow()
{
    if (hwnd) {
        CloseWindow(hwnd);
        UnregisterClass(wcls_name.c_str(), Application::instance());
    }
}

void MainWindow::show(int cmdShow)
{
    ShowWindow(hwnd, cmdShow);
    UpdateWindow(hwnd);
}

MainWindow::MainWindow(std::string name, DWORD style)
{
    DWORD err;

    if (!name.size())
        { throw std::runtime_error("MainWindow::MainWindow(): incorrect parameter value!"); }

    constructed = false;
    wcls_name = std::string("cls_") + name;

    memset(&wcls, 0, sizeof(wcls));
    wcls.cbSize = sizeof(wcls);
    wcls.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
    wcls.lpfnWndProc = (WNDPROC) MainWindow::wndProc;
    wcls.hInstance = Application::instance();
    wcls.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcls.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcls.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcls.lpszClassName = wcls_name.c_str();
    wcls.hIconSm = NULL;

    ATOM ret = RegisterClassEx(&wcls);
    if (!ret) {
        err = GetLastError();
        goto error_cls;
    }

    hwnd = CreateWindow(wcls_name.c_str(), name.c_str(), style,
                        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, 0, 0, Application::instance(), this);
    if (!hwnd) {
        err = GetLastError();
        goto error_wnd;
    }

    hButton1 = CreateWindow("BUTTON", "Running1", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,
                            button1.x, button1.y, button1.w, button1.h,
                            hwnd, (HMENU)ID_BUTTON1, Application::instance(), NULL);
    if (!hButton1) {
        err = GetLastError();
        goto error_but1;
    }

    hButton2 = CreateWindow("BUTTON", "Running2", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,
                            button2.x, button2.y, button2.w, button2.h,
                            hwnd, (HMENU)ID_BUTTON2, Application::instance(), NULL);
    if (!hButton2) {
        err = GetLastError();
        goto error_but2;
    }
    constructed = true;
    return;

error_but2:
    DestroyWindow(hButton1);
error_but1:
    DestroyWindow(hwnd);
error_wnd:
    UnregisterClass(wcls_name.c_str(), Application::instance());
error_cls:
    throw system_error(err);
}

LRESULT MainWindow::windowProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg) {
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:
            if (constructed) {
                PostQuitMessage(0);
            }
            break;
        case WM_COMMAND:
            if (HIWORD(wParam) == BN_CLICKED)
            {
                switch (LOWORD(wParam)) {       // реакция на нажатие кнопок
                    case ID_BUTTON1:
                        if (onButton1Click) {
                            onButton1Click(hButton1);
                            SetWindowText(hButton1, "Stopped1");
                        }
                        break;
                    case ID_BUTTON2:
                        if (onButton2Click) {
                            onButton2Click(hButton2);
                            SetWindowText(hButton2, "Stopped1");
                        }
                        break;
                }
            }
            break;
        case WM_MOVEBUTTON1:                 // реакция на события перемещения
            if (onButton1Move) { onButton1Move(hButton1); }
            break;
        case WM_MOVEBUTTON2:
            if (onButton2Move) { onButton2Move(hButton2); }
            break;
        case WM_MOVEBUTTONS12:
            if (onButtons12Move) { onButtons12Move(hButton1, hButton2); }
            break;

        default:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK MainWindow::wndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    if (uMsg == WM_CREATE) {
        CREATESTRUCT *ptr = reinterpret_cast<CREATESTRUCT*>(lParam);
        MainWindow *pmain = reinterpret_cast<MainWindow*>(ptr->lpCreateParams);
        SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)pmain);
    } else {
        LONG_PTR pmain = GetWindowLongPtr(hwnd, GWLP_USERDATA);
        if (pmain) {
           return (reinterpret_cast<MainWindow*>(pmain))->windowProc(uMsg, wParam, lParam);
        }
    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

bool MainWindow::userTranslator(MSG* msg)
{
    if (msg && (msg->message == WM_MOVEBUTTON1 ||
                msg->message == WM_MOVEBUTTON2 ||
                msg->message == WM_MOVEBUTTONS12))
    {
        msg->hwnd = hwnd;
        return true;
    }
    return false;
}



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPTSTR, int cmdShow)
{
    Application app(hInstance, "Task_3");

    int retcode = -1;
    int m3_off = 1;
    try {
        // создание главного окна
        MainWindow main(app.name(), WS_OVERLAPPEDWINDOW);
        main.show(cmdShow);

        // потоки, таймауты и события
        Ticker m1(TM_BUTTON1,   WM_MOVEBUTTON1);
        Ticker m2(TM_BUTTON2,   WM_MOVEBUTTON2);
        Ticker m3(TM_BUTTONS12, WM_MOVEBUTTONS12);

        // Задаем действия на нажатия кнопок
        main.onButton1Click = [&m1](HWND) { m1.stop(); };
        main.onButton2Click = [&m2](HWND) { m2.stop(); };

        // Действия на события перемещения
        main.onButton1Move = [](HWND hButton) { moveWindowByOff(hButton, 1); };
        main.onButton2Move = [](HWND hButton) { moveWindowByOff(hButton, 1); };
        main.onButtons12Move = [&m3_off](HWND hButton1, HWND hButton2) {
            m3_off = (~m3_off)+1;
            moveWindowByOff(hButton1, m3_off);
            moveWindowByOff(hButton2, m3_off);
        };

        // передача наших событий (WM_MOVE**) окну
        app.userTranslation = [&main](MSG* msg) -> bool { return main.userTranslator(msg); };

        // обработка событий приложения
        retcode = app.messageLoop();
    } catch(std::runtime_error& err) {
        MessageBox(NULL, err.what(), app.name().c_str(), MB_ICONERROR | MB_OK);
    }
    return retcode;
}
